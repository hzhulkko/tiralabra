
package regex.domain;

import regex.domain.State;
import regex.domain.Edge;

/**
 * Luokka kuvaa kaaren, joka vastaa mitä tahansa merkkiä.
 */

public class AnyCharEdge implements Edge {

    /**Kaaren kohdetila*/
    private State target;
    /**Ilmaisee, onko kaari osa laskuritilaan liittyvää silmukkaa*/
    private boolean counting;
    
    /**
     * Konstruktori sa parametrina kaaren kohdetilan.
     * @param target kohdetila
     */
    public AnyCharEdge(State target) {
        this.target = target;
        this.counting = false;
    }

    public AnyCharEdge() {
        this(null);
    }
    
    @Override
    public char getC() {
        return '.';
    }

    @Override
    public State getTarget() {
        return target;
    }

    @Override
    public void setTarget(State target) {
        this.target = target;
    }

    @Override
    public boolean matches(char c) {
        return true;
    }

    @Override
    public void makeCounting() {
        this.counting = true;
    }

    @Override
    public boolean isCounting() {
        return this.counting;
    }

    
}
