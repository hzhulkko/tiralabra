
package regex.matcher;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**Luokka toteuttaa säännöllisten lausekkeiden tulkin Javan Pattern- ja Matcher-
 * luokkien avulla. Luokka toteuttaa rajapinnan RegexMatcher.
 * @see RegexMatcher
 * @see java.util.regex.Pattern
 * @see java.util.regex.Matcher
 */
public class StandardMatcher implements RegexMatcher {
    
    /** Säännöllisen lausekkeen tunnistaja
     * @see java.util.regex.Matcher*/
    private Matcher matcher;
    /**Säännöllisen lausekkeen kääntäjä
     * @see java.util.regex.Pattern*/
    private Pattern pattern;
    
    /**Luo tyhjän säännöllisen lausekkeen tulkin.*/
    public StandardMatcher() {
        this.pattern = null;
    }

    /**
     * Luo tulkin annetulle säännölliselle lausekkeelle. Konstruktori kääntää String-tyyppisen 
     * säännöllisen lausekkeen Pattern-luokan compile()-metodin avulla
     * @param regex säännöllinen lauseke
     * @see java.util.regex.Pattern#compile(java.lang.String) Pattern.compile
     */
    public StandardMatcher(String regex) {
        this.pattern = Pattern.compile(regex);
    }
    
    @Override
    public void setRegex(String regex) {
        this.pattern = Pattern.compile(regex);
    }
    
    @Override
    public boolean find(int i, String line) {
        if (pattern == null) {
            throw new IllegalStateException();
        }
        matcher = pattern.matcher(line);
        return matcher.find(i);
    }
    
    @Override
    public int start() {
        return matcher.start();
    }
    
    @Override
    public int end() {
        return matcher.end();
    }
    
}
