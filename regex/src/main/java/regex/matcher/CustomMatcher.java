
package regex.matcher;


public class CustomMatcher implements RegexMatcher {
    
    private Nfa pattern;
    private NfaMatcher matcher;
    
    public CustomMatcher() {
        this.pattern = new Nfa();
        this.matcher = null;
    }
    
    public CustomMatcher(String regex) {
        this.pattern = new Nfa();
        this.pattern.compile(regex);
        this.matcher = pattern.matcher();
    }

    @Override
    public int end() {
        return matcher.end();
    }

    @Override
    public boolean find(int i, String line) {
        return matcher.find(i, line);
    }

    @Override
    public int start() {
        return matcher.start();
    }

    @Override
    public void setRegex(String regex) {
        pattern.compile(regex);
        matcher = pattern.matcher();
    }
    
}