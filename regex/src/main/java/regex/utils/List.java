
package regex.utils;

/**
 * Luokka toteuttaa geneerisen listan taulukon avulla. Listalle lisättävät elementit
 * lisätään taulukkoon. Kun taulukko täyttyy, sen koko kaksinkertaistetaan.
 * @param <E> 
 */

public class List<E> {
    /**Elementit säilövä taulukko*/
    private Object[] elements;
    /**Lisättyjen elementtien lukumäärä*/
    private int size;
    /**Taulukon alkukapasiteetti*/
    private static final int CAPACITY = 10;
    
    /**
     * Luo listan, jonka alkukapasiteetti on annettu-
     * @param capacity alkukapasiteetti 
     */
    public List(int capacity) {
        this.elements = new Object[capacity];
        this.size = 0;
    }
    /**
     * Luo listan, jonka alkukapasiteetti on 10.
     */
    public List() {
        this(CAPACITY);
    }
    /**
     * Palauttaa listassa olevien elementtien lukumäärän.
     * @return listan elementtien lukumäärä
     */
    public int size() {
        return size;
    }
    /**
     * Ilmaisee onko lista tyhjä
     * @return true jos lista on tyhjä, muulloin false
     */
    public boolean isEmpty() {
        return size == 0;
    }
    
    /**
     * Lisää elementin e listan taulukon loppuun. Jos taulukko on täynnä, 
     * kaksinkertaistaa taulukon koon ja kopioi aikaisemmat arvot uuteen taulukkoon. 
     * @param e 
     */
    public void add(E e) {
        if (e == null) return;
        if (size == elements.length) {
            copyAndDouble();
        }
        elements[size] = e;
        size++;
    }
    
    /**
     * Yhdistää kaksi listaa.
     * @param list yhdistettävä lista 
     */
    public void addAll(List<E> list) {
        for (int i = 0; i < list.size; i++) {
            add(list.get(i));
        }
    }
    /**
     * Palauttaa listan taulukossa i olevan elementin E.
     * @param i taulukon indeksi
     * @return elementti indektissä i
     */
    public E get(int i) {
        if (i < 0 || i >= size) {
            throw new ArrayIndexOutOfBoundsException();
        }
        return (E) elements[i];
    }
    
    /**Tyhjentää listan luomalla tilalle uuden taulukon*/
    public void clear() {
        elements = new Object[elements.length];
        size = 0;
    }
    /**
     * Luo uuden alkuperäistä kaksi kertaa suuremman taulukon ja lisää alkuperäisen
     * taulukon elementit uuteen taulukkoon alkuperäisille indekseille.
     */
    private void copyAndDouble() {
        Object[] arr = new Object[size * 2];
        for (int i = 0; i < size; i++) {
            arr[i] = elements[i];
        }
        elements = arr;
    }
    
}
