
package regex.utils;

/**
 * Luokka kuvaa geneerisen jonon, joka on toteutettu linkitetyn listan avulla.
 */
public class Queue<T> {

    private int size;
    private Node<T> head;
    private Node<T> tail;

    public Queue() {
        this.size = 0;
        this.head = new Node<T>(null);
        this.tail = head;
    }
    /**
     * Liittää tyypin T alkion jonon viimeiseksi.
     */
    public void enqueue(T t) {
        Node<T> item = new Node<T>(t);
        if (isEmpty()) {
            head.setNext(item);
            item.setPrev(head);
        } else {
            tail.setNext(item);
            item.setPrev(tail);
        }
        tail = item;
        size++;
    }
    /**
     * Poistaa ja palauttaa jonon ensimmäisen alkion.
     */
    public T dequeue() {
        if (isEmpty()) {
            throw new NullPointerException();
        }
        Node<T> removed = head.getNext();
        Node<T> next = removed.getNext();
        
        if (next != null) {
            head.setNext(next);       
            next.setPrev(head);
        } else {
            head.setNext(null);
            tail = head;
        }
  
        size--;
        return removed.get();
    }
   /**
    * Ilmaisee onko jono tyhjä.
    * @return true jos jonossa ei ole alkiota, muulloin false */
   public boolean isEmpty() {
       return head == tail;
   }
   /** 
    * Palauttaa jonossa olevien alkioiden lukumäärän
    * @return alkioiden lukumäärä, 0 jos jono on tyjä */
   public int size() {
       return size;
   }

}
