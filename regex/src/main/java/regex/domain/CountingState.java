
package regex.domain;

import regex.utils.List;

/**
 * Luokka toteuttaa haarautuvan tilan, joka valvoo vapaaseen kaareen liityvän tilan käyntikertoja. 
 * Tilalle voi asettaa sallittujen käyntikertojen minimi- ja maksimiarvot. Toinen, rajoitettu
 * kaari sallii kulun seuraavaan tilaan, jos toteutuneet käyntikerrat ovat sallittujen rajojen sisällä.
 */
public class CountingState extends State {
    
    /**Sallittujen käyntikertojen miniminmäärä*/
    private int min;
    /**Sallittujen käyntikertojen maksimimäärä*/
    private int max;
    /**Valvotun tilan käyntikertojen lukumäärä*/
    private int visits;
    /**Vapaa kaari*/
    private Edge free;
    /**Rajoitettu kaari*/
    private Edge restricted;
    
    public CountingState() {
        super(3);
        this.min = -1;
        this.max = -1;
        this.visits = -1;
    }
    
    /**
     * Palauttaa sallittujen käyntien minimimäärän.
     * @return käyntien minimimäärä
     */
    public int getMin() {
        return min;
    }
    /**
     * Asettaa sallittujen käyntien minimimäärän.
     * @param min minimimäärä
     */
    public void setMin(int min) {
        this.min = min;
    }
    
    /**
     * Palauttaa sallittujen käyntien maksimimäärän.
     * @return käyntien maksimimäärä
     */
    public int getMax() {
        return max;
    }
    
    /**
     * Asettaa sallittujen käyntien maksimimäärän.
     * @param max maksimimäärä
     */
    public void setMax(int max) {
        this.max = max;
    }
    
    /**
     * Lisää tilaan lähtevän kaaren.
     * @param edge lähtevä kaari
     * @see Edge
     */
    @Override
    public void add(Edge edge) {
        if (free == null) {
            free = edge;
        } else {
            restricted = edge;
        }
    }

    /**
     * Palauttaa listan lähtevistä kaarista. Jos parametrina annettu käyntikertojen lukumäärä
     * on pienempi tai yhtä suuri kuin sallittu käyntikertojen maksimimäärä, sisällytetään listaan vapaa kaari.
     * Rajoitettu kaari sisältyy listaan, jos käyntikertojen lukumäärä on lisäksi suurempi 
     * tai yhtä suuri kuin sallittu käyntikertojen minimimäärä. Muulloin lista on tyhjä.
     * @param counter käyntikertojen lukumäärä
     * @return lista kaarista, tyhjä lista jos counter > maksimi 
     */
    @Override
    public List<Edge> getEdges(int counter) {
        
        List<Edge> list = new List<Edge>(2);
        if (counter <= max || max == -1) {
            list.add(free);
        }
        if (enoughVisits(counter)) {
            list.add(restricted);
        }

        return list;
        
    }
    
    /**Vertaa käyntikertojen lukumäärää raja-arvoihin*/
    private boolean enoughVisits(int counter) {
        if (max == -1) {
            return counter >= min;
        } else {
            return ((counter >= min) && (counter <= max));
        }
    }
    
}
