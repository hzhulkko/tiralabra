/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package regex.matcher;

import org.junit.Test;
import static org.junit.Assert.*;

public class StandardMatcherTest {
    
    private RegexMatcher standard;
    private RegexMatcher custom;
    
    
    public void setUpBoth(String regex) {
        standard = new StandardMatcher(regex);
        custom = new CustomMatcher(regex);     
    }
    
    @Test
    public void matcherFindsLiterals() {
        String regex = "aaa";
        setUpBoth(regex);
        String test1 = "aaa";
        assertTrue(standard.find(0, test1));
        assertTrue(custom.find(0, test1));
    }
    
    @Test
    public void matcherFindsLiterals2() {
        String regex = "aaa";
        setUpBoth(regex);
        String test1 = "baaa";
        assertTrue(standard.find(0, test1));
        assertTrue(custom.find(0, test1));
    }
    
    @Test
    public void matcherFindsAlternatives() {
        String regex = "a[bc]a";
        setUpBoth(regex);
        String test1 = "aba";
        String test2 = "aca";
        assertTrue(standard.find(0, test1) && standard.find(0, test2));
        assertTrue(custom.find(0, test1) && standard.find(0, test2));
    }
    
    @Test
    public void matcherFindsAlternatives2() {
        String regex = "ab|cd";
        setUpBoth(regex);
        String test1 = "ab";
        String test2 = "ac";
        String test3 = "cd";
        assertTrue(standard.find(0, test1) && !standard.find(0, test2) && standard.find(0, test3));
        assertTrue(custom.find(0, test1) && !custom.find(0, test2) && custom.find(0, test3));
    }
    
    @Test
    public void matcherFindsAlternatives3() {
        String regex = "a[bc-]d";
        setUpBoth(regex);
        String test1 = "abd";
        String test2 = "acd";
        String test3 = "a-d";
        assertTrue(standard.find(0, test1) && standard.find(0, test2) && standard.find(0, test3));
        assertTrue(custom.find(0, test1) && custom.find(0, test2) && custom.find(0, test3));
    }
    
    @Test
    public void matcherIgnoresUnmatchingStrings() {
        String regex = "a[bc]a";
        setUpBoth(regex);
        String test1 = "aaa";
        assertFalse(standard.find(0, test1));
        assertFalse(custom.find(0, test1));
    }
    
    @Test
    public void matcherFindsQantities() {
        String regex = "ab*a";
        setUpBoth(regex);
        String test1 = "aa";
        String test2 = "aba";
        String test3 = "abbbbbba";
        assertTrue(standard.find(0, test1) && standard.find(0, test2) && standard.find(0, test3));
        assertTrue(custom.find(0, test1) && custom.find(0, test2) && custom.find(0, test3));
    }
    
    @Test
    public void matcherFindsQantities2() {
        String regex = "ab+a";
        setUpBoth(regex);
        String test1 = "aa";
        String test2 = "aba";
        String test3 = "abbbbbba";
        assertTrue(!standard.find(0, test1) && standard.find(0, test2) && standard.find(0, test3));
        assertTrue(!custom.find(0, test1) && custom.find(0, test2) && custom.find(0, test3)); 
    }
    
    @Test
    public void matcherFindsQantities3() {
        String regex = "ab?a";
        setUpBoth(regex);
        String test1 = "aa";
        String test2 = "aba";
        String test3 = "abbbbbba";
        assertTrue(standard.find(0, test1) && standard.find(0, test2) && !standard.find(0, test3));
        assertTrue(custom.find(0, test1) && custom.find(0, test2) && !custom.find(0, test3)); 
    }
    
    @Test
    public void matcherFindsQantities4() {
        String regex = "a(bc*)a";
        setUpBoth(regex);
        String test1 = "aa";
        String test2 = "aba";
        String test3 = "abccca";
        assertTrue(!standard.find(0, test1) && standard.find(0, test2) && standard.find(0, test3));
        assertTrue(!custom.find(0, test1) && custom.find(0, test2) && custom.find(0, test3)); 
    }
    
    @Test
    public void matcherFindsQantities5() {
        String regex = "ab{1,3}c";
        setUpBoth(regex);
        String test1 = "aa";
        String test2 = "abbc";
        String test3 = "ababbc";
        assertTrue(!standard.find(0, test1) && standard.find(0, test2) && standard.find(0, test3));
        assertTrue(!custom.find(0, test1) && custom.find(0, test2) && custom.find(0, test3)); 
    }
    
    @Test
    public void matcherFindsQantities6() {
        String regex = "a{1,3}b{3,3}";
        setUpBoth(regex);
        String test1 = "ab";
        String test2 = "abbb";
        String test3 = "aaabbb";
        assertTrue(!standard.find(0, test1) && standard.find(0, test2) && standard.find(0, test3));
        assertTrue(!custom.find(0, test1) && custom.find(0, test2) && custom.find(0, test3)); 
    }
    
    @Test
    public void matcherFindsQuantities7() {
        setUpBoth("a((bc){1,2}|d{3,5})");
        String test1 = "abababcbcgg";
        String test2 = "adadadddgg";

        assertTrue(standard.find(0, test1) && standard.find(0, test2));
        assertTrue(custom.find(0, test1) && custom.find(0, test2));

    }
    
    @Test
    public void findStartsAtGivenIndex() {
        String regex = "ab?a";
        setUpBoth(regex);
        String test3 = "abbbbbba";
        assertFalse(standard.find(3, test3));
        assertFalse(custom.find(3, test3));
    }
    
    @Test
    public void matcherChangesRegex() {
        setUpBoth("dd");
        standard.setRegex("ab?a");
        custom.setRegex("ab?a");
        String test1 = "aa";
        String test2 = "aba";
        String test3 = "abbbbbba";
        assertTrue(standard.find(0, test1) && standard.find(0, test2) && !standard.find(0, test3)); 
        assertTrue(custom.find(0, test1) && custom.find(0, test2) && !custom.find(0, test3));
    }
    
    @Test
    public void matcherFindsStartAndEndIndeces() {
        setUpBoth("zz");
        String test1 = "aaazzbbb";
        standard.find(0, test1);
        custom.find(0, test1);
        
        assertEquals(3, standard.start());
        assertEquals(3, custom.start());
        assertEquals(5, standard.end());
        assertEquals(5, custom.end());
        
    }
    
    @Test
    public void matcherFindsStartAndEndIndeces2() {
        setUpBoth("abz*");
        String test1 = "abzzzzzz";
        standard.find(0, test1);
        custom.find(0, test1);
        
        assertEquals(0, standard.start());
        assertEquals(0, custom.start());
        assertEquals(8, standard.end());
        assertEquals(8, custom.end());
        
        
    }

    @Test
    public void matcherFindsStartAndEndIndeces3() {
        setUpBoth("abz*");
        String test1 = "aaabzzzzdd";
        standard.find(0, test1);
        custom.find(0, test1);

        assertEquals(2, standard.start());
        assertEquals(2, custom.start());
        assertEquals(8, standard.end());
        assertEquals(8, custom.end());

    }

    @Test
    public void matcherFindsStartAndEndIndeces4() {
        setUpBoth("abz{2,2}");
        String test1 = "aaabzzzz";
        standard.find(0, test1);
        custom.find(0, test1);

        assertEquals(2, standard.start());
        assertEquals(2, custom.start());
        assertEquals(6, standard.end());
        assertEquals(6, custom.end());

    }
    
    @Test
    public void matcherFindsStartAndEndIndeces5() {
        setUpBoth("tu{1,2}li");
        String test1 = "tuuli kohtuiseksi";
        standard.find(0, test1);
        custom.find(0, test1);

        assertEquals(0, standard.start());
        assertEquals(0, custom.start());
        assertEquals(5, standard.end());
        assertEquals(5, custom.end());

    }
    
    @Test
    public void matcherFindsStartAndEndIndeces6() {
        setUpBoth("abz+");
        String test1 = "abzzzzzz";
        standard.find(0, test1);
        custom.find(0, test1);
        
        assertEquals(0, standard.start());
        assertEquals(0, custom.start());
        assertEquals(8, standard.end());
        assertEquals(8, custom.end());
        
        
    }

    @Test
    public void matcherFindsStartAndEndIndeces7() {
        setUpBoth("abz+");
        String test1 = "aaabzzzzdd";
        standard.find(0, test1);
        custom.find(0, test1);

        assertEquals(2, standard.start());
        assertEquals(2, custom.start());
        assertEquals(8, standard.end());
        assertEquals(8, custom.end());

    }
    
    @Test
    public void matcherFindsStartAndEndIndeces8() {
        setUpBoth("a*b*c*d*");
        String test1 = "aabcdddd";
        standard.find(0, test1);
        custom.find(0, test1);

        assertEquals(0, standard.start());
        assertEquals(0, custom.start());
        assertEquals(8, standard.end());
        assertEquals(8, custom.end());

    }
    
    @Test
    public void matcherFindsStartAndEndIndeces9() {
        setUpBoth("aa");
        String test1 = "aaa";
        standard.find(0, test1);
        custom.find(0, test1);

        assertEquals(0, standard.start());
        assertEquals(0, custom.start());
        assertEquals(2, standard.end());
        assertEquals(2, custom.end());

    }
    
    @Test
    public void matcherFindsStartAndEndIndeces10() {
        setUpBoth("aa");
        String test1 = "aaa";
        standard.find(0, test1);
        custom.find(0, test1);

        assertEquals(0, standard.start());
        assertEquals(0, custom.start());
        assertEquals(2, standard.end());
        assertEquals(2, custom.end());

    }
    
    @Test
    public void matcherFindsStartAndEndIndeces11() {
        //setUpBoth("TT[GA].{3,3}T.{16,18}[TC].TAA[TAC]");
        setUpBoth("TT[GA].{3,3}T.{16,18}");
        String test1 = "CTTATTGTCTTCATTATACTTAATTTTGTAATTAAGT";
        standard.find(0, test1);
        custom.find(0, test1);

        assertEquals(1, standard.start());
        assertEquals(1, custom.start());
        assertEquals(26, standard.end());
        assertEquals(26, custom.end());

    }
    
    //TGTTAAATCGGCCATAACTGGCTGTATTACTGTTATTATTGCTGTCATAAGAGCGATACTGACTTAAATT
    @Test
    public void matcherFindsStartAndEndIndeces12() {
        setUpBoth("TT[GA].{3,3}T.{16,18}[TC].TAA[TAC]");

        String test1 = "TGTTAAATCGGCCATAACTGGCTGTATTACTGTTATTATTGCTGTCATAAGAGCGATACTGACTTAAATT";
        standard.find(0, test1);
        custom.find(0, test1);

        assertEquals(38, standard.start());
        assertEquals(38, custom.start());
        assertEquals(68, standard.end());
        assertEquals(68, custom.end());

    }
    
    
    
    
    @Test(expected = IllegalStateException.class)
    public void nullPatternThrowsException() {
        standard = new StandardMatcher();
        String test1 = "aa";
        standard.find(0, test1);
    }
    
}
