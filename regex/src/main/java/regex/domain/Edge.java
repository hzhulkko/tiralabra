
package regex.domain;

/**Luokka kuvaa kaarirajapinnan*/

public interface Edge {
    /**
     * Palauttaa kaaren merkin.
     * @return kaaren merkki
     */
    char getC();

    /**
     * Palauttaa kaaren kohdetilan.
     * @return kohdetila
     * @see State
     */
    State getTarget();

    /**
     * Asettaa kaarelle kohdetilan.
     * @param target kohdetila
     * @see State
     */
    void setTarget(State target);
    
    /**
     * Tarkista vastaako kaaren merkki annettua merkkiä.
     * @param c annettu merkki
     * @return true, jos kaaren merkki vastaa annettua merkkiä, muulloin false
     */
    boolean matches(char c);
    /**
     * Merkitsee kaaren osaksi laskuritilaan liittyvää silmukkaa.
     */
    void makeCounting();
    /**
     * Ilmaisee, onko kaari osa laskuritilaan liittyvää silmukkaa.
     * @return true jos kaari liittyy laskuritilan silmukkaan, muulloin false.
     */
    boolean isCounting();
    
}
