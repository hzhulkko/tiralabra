
package regex.utils;

import java.util.Arrays;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class StackTest {
    
    private Stack<Integer> s;
    
    @Before
    public void setUp() {
        s = new Stack<Integer>();
    }
    
    @Test
    public void constructorCreatesEmptyStack() {
        assertTrue(s.isEmpty() && s.size() == 0);
    }
    
    @Test
    public void stackIsNotEmptyAfterPush() {
        s.push(1);
        assertFalse(s.isEmpty() && s.size() == 1);
    }
    
    @Test(expected = NullPointerException.class)
    public void popThrowsException() {
        s.pop();
    }
    
    @Test
    public void pushIncreasesStackSize() {
        for (int i = 1; i < 6; i++) {
            s.push(i);
        }
        assertEquals(5, s.size());
    }
    
    @Test
    public void popDecreasesStackSize() {
        s.push(1);
        s.push(2);
        s.push(3);
        s.pop();
        assertEquals(2, s.size());
    }
    
    @Test
    public void popReturnsFirstOnStack() {
        s.push(1);
        s.push(2);
        int k = s.pop();
        assertEquals(2, k);
    }
    
    @Test
    public void popReturnsItemsInRightOrder() {
        for (int i = 1; i <= 10; i++) {
            s.push(i);
        }
        int[] list = new int[10];
        for (int i = 0; i < 10; i++) {
            list[i] = s.pop();
        }
        
        assertEquals(Arrays.toString(list), "[10, 9, 8, 7, 6, 5, 4, 3, 2, 1]");
    }
    
    // AIKAVAATIVUUSTESTIT
    
    @Test
    public void pushTimeTest() {
        double average = calcAveragePushTime(100);
        double delta = average;
        
        double test = calcAveragePushTime(100);
        
        assertEquals(average, test, delta);
        
        test = calcAveragePushTime(1000);
        
        assertEquals(average, test, delta);
        
        test = calcAveragePushTime(10000);
        
        assertEquals(average, test, delta);
        
        test = calcAveragePushTime(100000);
        
        assertEquals(average, test, delta);
        
        test = calcAveragePushTime(100000);
        
        assertEquals(average, test, delta);

    }
    
    @Test
    public void popTimeTest() {
        double average = calcAveragePopTime(100);
        double delta = average;
        
        double test = calcAveragePopTime(100);
        
        assertEquals(average, test, delta);
        
        test = calcAveragePopTime(1000);
        
        assertEquals(average, test, delta);
        
        test = calcAveragePopTime(10000);
        
        assertEquals(average, test, delta);
        
        test = calcAveragePopTime(100000);
        
        assertEquals(average, test, delta);
        
        test = calcAveragePopTime(1000000);
        
        assertEquals(average, test, delta);
        
    }
    
    
    private double calcAveragePushTime(int max) {
        Stack<Integer> stack = new Stack<Integer>();
        
        double sum = 0.0;
        
        for (int n = 0; n < max; n++) {
            double start = getCurrentTime();
            stack.push(1);
            double stop = getCurrentTime();
            sum += stop - start;      
        }
        return sum / max;
    }
    
    private double calcAveragePopTime(int max) {
        Stack<Integer> stack = new Stack<Integer>();
        
        for (int n = 0; n < max; n++) {
            stack.push(1);
        }
        
        double sum = 0.0;
        
        for (int n = 0; n < max; n++) {
            double start = getCurrentTime();
            stack.pop();
            double stop = getCurrentTime();
            sum += stop - start;      
        }
        return sum / max;
    }
    
    private double getCurrentTime() {
        return System.nanoTime();
    }
    
    
    
    
}
