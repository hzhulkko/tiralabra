/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package regex;

import regex.io.LineReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import static org.junit.Assert.*;

public class LineReaderTest {
    
    private File testFile;
    private LineReader reader;
    
    @Rule
    public TemporaryFolder tmp = new TemporaryFolder();
    
    
    @Before
    public void setUp() {
        try {
            testFile = tmp.newFile("test.txt");
        } catch (IOException ex) {
        }
        reader = new LineReader(testFile);
    }
    
    @Test
    public void readLineReturnsFirstLine() {
        write("aaa\nbbb\nccc");
        String line = reader.readLine();
        assertTrue(line.equals("aaa"));
    }
    
    @Test
    public void readLineReturnsAllLines() {
        write("aaa\nbbb\nccc");
        ArrayList<String> list = new ArrayList<String>();
        String line = reader.readLine();
        while (line != null) {
            list.add(line);
            line = reader.readLine();
        }
        assertEquals(list.toString(), "[aaa, bbb, ccc]");
    }
    
    @Test
    public void readLineCountsReadLines() {
        write("aaa\nbbb\nccc");
        String line = reader.readLine();
        while (line != null) {
            line = reader.readLine();
        }
        assertEquals(3, reader.getLineNumber());
    }
    
    @Test
    public void resetResetsReader() {
        write("aaa\nbbb\nccc");
        String line = reader.readLine();
        while (line != null) {
            line = reader.readLine();
        }
        reader.reset();
        assertEquals(0, reader.getLineNumber());
    }
    
    @Test
    public void readerStartsFromBeginningAfterReset() {
        write("aaa\nbbb\nccc");
        String line = reader.readLine();
        while (line != null) {
            line = reader.readLine();
        }
        reader.reset();
        line = reader.readLine();
        assertTrue(line.equals("aaa"));
    }
    
        
    private void write(String text) {
        FileWriter writer;
        try {
            writer = new FileWriter(testFile);
            writer.append(text);
            writer.close();
        } catch (IOException ex) {
        }
        
    }
}
