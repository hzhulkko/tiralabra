/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package regex;

import regex.matcher.CustomMatcher;
import regex.matcher.RegexMatcher;
import regex.matcher.StandardMatcher;

/**
 * Luokka sisältää metodeja aikavaativuustestien suorittamiseen.
 */
public class ATesting {
         
    private static String buildRegex(int n) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < n; i++) {
            sb.append("a?");
        }
        for (int i = 0; i < n; i++) {
            sb.append("a");
        }
        return sb.toString();
    }
    
    private static String buildTestString(int n) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < n; i++) {
            sb.append("a");
        }
        return sb.toString();
    }
    
    public static void test(int... stringLengths) {
        for (int i = 0; i < stringLengths.length; i++) {
            int n = stringLengths[i];
            String testString = buildTestString(n);
            String regex = buildRegex(n);
            
            RegexMatcher standard = new StandardMatcher(regex);
            System.out.println("\nTesting StandardMatcher: string length " + stringLengths[i]);
            System.out.println("Results: ");
            testMatcher(standard, testString, 5);
            
            RegexMatcher custom = new CustomMatcher(regex);
            System.out.println("\nTesting CustomMatcher: string length " + stringLengths[i]);
            System.out.println("Results: ");
            testMatcher(custom, testString, 5);
            
        }
        
    }
    
    private static void testMatcher(RegexMatcher matcher, String testString, int repeats) {
        double[] results = new double[repeats];
        for (int i = 0; i< repeats; i++) {
            double start = getCurrentTime();
            for (int j = 0; j < 100; j++) {
                boolean b = matcher.find(0, testString);
                //if (b) System.out.println(b + " " + matcher.start() + "..." + matcher.end());
                }
            double stop = getCurrentTime();
            results[i] = (stop - start)/100;
        }
        printResults(results);
    }
    
    private static void printResults(double[] results) {
        double sum = 0;
        
        for (int i = 0; i < results.length; i++) {
            System.out.print(results[i] + " us ");
            sum += results[i];
        }
        System.out.println("\nAverage time of find() call with " + results.length + " repeats " + (sum/results.length));
    }
    
    private static double getCurrentTime() {
        return System.nanoTime()/1000;
    }
    
    
    
}
