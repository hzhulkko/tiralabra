
package regex.matcher;

import regex.io.StringReader;
import regex.domain.CountingState;

/**
 * Luokka toteuttaa säännöllisen lausekkeen jäsentelijän, joka palauttaa käyntikertoja
 * laskevan tila-automaatin osan. Jäsentelijä lukee ja tulkitsee aaltosulkeilla
 * merkittyä säännöllisen lausekkeen osaa. Merkki '}' päättää luennan.
 * Esimerkkejä jäsentelijän tulkitsemista säännöllisen lausekkeen osista:
 * 
 * 1,3} = sallitaan 1-3 käyntiä
 * 4} = salliitaan tasan 4 käyntiä
 * 20,} = sallitaan vähintään 20 käyntiä
 */

public class CountParser implements Parser {
    
    /**Ilmaisee, onko luenta päättynyt*/
    private boolean quit;
    /**Ilmaisee, onko sallituilla käyntikerroilla kaksi raja-arvoa*/
    private boolean comma;
    /**Pitää lukua raja-arvojen kymmenpotensseista*/
    private int deg;
    /**Säännöllisen lausekkeen lukija*/
    private StringReader reader;
    /**Käyntien lukumäärää lskeva kaari*/
    private CountingState state;

    public CountParser(StringReader reader) {
        this.reader = reader;
        this.quit = false;
        this.comma = false;
        this.deg = 0;
        this.state = new CountingState();
    }
    
    @Override
    public void read() {
        while (!quit && reader.hasChar()) {
            parse(reader.getChar());
        }      
    }

    @Override
    public void parse(char c) {
        switch(c) {
            case '}':
                quit = true;
                break;
            case ',':
                comma = true;
                deg = 0;
                break;
            default:
                int value = Character.getNumericValue(c);
                
                if (state.getMin() == -1) {
                    state.setMin(value);
                } else if (!comma) {
                    int sum = (int)(Math.pow(10, deg) * state.getMin()) + value;
                    state.setMin(sum);
                } else if (state.getMax() == -1) {
                    state.setMax(value);
                } else {
                    int sum = (int)(Math.pow(10, deg) * state.getMax()) + value;
                    state.setMax(sum);
                }
                deg++;
                break;
        }
    }

    @Override
    public Fragment getFragment() {
        if (!comma && state.getMax() == -1) {
            state.setMax(state.getMin());
        }
        return new Fragment(state);
    }

    
}
