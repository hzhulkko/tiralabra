package regex.matcher;

import regex.domain.State;
import regex.domain.Edge;
import regex.utils.List;

/**
 * Luokka toteuttaa tiloista ja kaarista muodostuvan tila-automaatin osan. Osat
 * ovat katenoituvia tai ei-katenoituvia: katenoituvien osien perään voidaan
 * liittää osia. Jos osa ei ole katenoituva, muut osat liitetään osan rinnalle.
 */
public class Fragment {

    /**Osan alkutila*/
    private State start;
    /**Lista ulospäin suuntautuvista kaarista*/
    private List<Edge> out;
    /**Ilmaisee onko osa katenoituva*/
    private boolean catenative;

    /**
     * Konstruktori saa parametrina alkutila, ulospäin suuntautuvat kaaret ja
     * tiedon katenoituvuudesta.
     *
     * @param start alkutila
     * @param out ulospäin suuntautuvat kaaret
     * @param c katenoituvuus
     */
    public Fragment(State start, List<Edge> out, boolean c) {
        this.start = start;
        this.catenative = c;
        this.copyEdges(out);
    }

    /**
     * Konstruktori saa parametrina alkutila ja ulospäin suuntautuvat kaaret.
     * Katenotuvuus on oletusarvoisesti true.
     *
     * @param start alkutila
     * @param out ulospäin suuntautuvat kaaret
     */
    public Fragment(State start, List<Edge> out) {
        this.start = start;
        this.catenative = true;
        this.copyEdges(out);
    }
    
    public Fragment(State start, Edge out) {
        this.start = start;
        this.catenative = true;
        this.out = new List<Edge>(1);
        this.out.add(out);
    }

    /**
     * Luo alkutilasta ja alkutilan kaarista osan, jonka katenoituvuus on määrätty.
     *
     * @param start alkutila
     * @param c katenoituvuus
     */
    public Fragment(State start, boolean c) {
        this.start = start;
        this.catenative = c;
        this.copyEdges(start.getEdges());

    }

    /**
     * Luo alkutilasta ja alkutilan kaarista muodostuvan katenoituvan osan.
     * @param start alkutila
     */
    public Fragment(State start) {
        this(start, true);
    }

    /**
     * Ilmaisee, onko osa katenoituva.
     * @return true, jos osa on katenoituva, muulloin false
     */
    public boolean isCatenative() {
        return catenative;
    }

    /**
     * Määrittää osan katenoituvuuden.
     * @param c
     */
    public void setCatenative(boolean c) {
        this.catenative = c;
    }

    /**
     * Lisää osaan kaaria.
     * @param list lista lisättävistä kaarista
     * @see Edge
     */
    public void append(List<Edge> list) {
        out.addAll(list);
    }

    /**
     * Liittää osan vapaat kaaret parametrina annettuun tilaan.      *
     * @param s kohdetila
     * @see State
     */
    public void patch(State s) {
        for (int i = 0; i < out.size(); i++) {
            Edge e = out.get(i);
            if (e.getTarget() == null) {
                e.setTarget(s);
            }
        }
        copyEdges(s.getEdges());

    }
    /**
     * Palauttaa osan alkutilan.
     * @return alkutila
     * @see State
     */
    public State getStart() {
        return start;
    }
    /**
     * Palauttaa listan kaarista.
     * @return lista kaarista
     * @see Edge
     */
    public List<Edge> getOut() {
        return out;
    }
    
    /**Tyhjentää ulospäin suuntautuvien kaarien listan ja lisää annetut kaaret listalle.
     * @param list lista liitettävistä kaaret
     */
    private void copyEdges(List<Edge> list) {
        out = new List<Edge>();
        out.addAll(list);
    }
    
    
    
    
}
