/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package regex.matcher;

import regex.domain.State;

/**
 * Luokka kuvaa tilaolioiden säiliön, joka pitää kirjaa kuljetun polun pituudesta ja
 * huolehtii laskuritiloihin liittyvistä käyntilaskureista. Tilasäiliö ei ole tilakohtainen,
 * vaan säiliö voi säilöä eri tiloja.
 */
public class StateContainer {
    
    /** Säiliön sisältämä tila*/
    private State state;
    /** Kuljetun polun pituus*/
    private int pathLength;
    /** Taulukko tilaan liittyville käyntilaskureille*/
    private int counters[];
    /** Osoitin, joka ilmaisee viimeisimmän laskurin sijainnin counters-taulukossa*/
    private int top;
  
    /**
     * Luo tilalle uuden säiliön, joka saa parametrina kuljetun polun pituuden ja 
     * laskuritaulukon koon. 
     * @param state säilöttävä tila
     * @param pathLength polun pituus
     * @param maxCounters laskuritaulukon koko
     */
    public StateContainer(State state, int pathLength, int maxCounters) {
        this.state = state;
        this.pathLength = pathLength;
        this.counters = new int[maxCounters];
        this.top = -1;
        
    }
    
    /**
     * Luo lisättävälle uuden säiliön, joka saa polun pituuden ja laskuritaulukon 
     * tilan edelliseltä säiliöltä.
     * @param state säiliöön lisättävä tila
     * @param previous tilan edellinen säiliö
     */
    public StateContainer(State state, StateContainer previous) {
        this(state, previous.pathLength, previous.counters.length);
        for (int i = 0; i < counters.length; i++) {
            counters[i] = previous.counters[i];
        }
        top = previous.top;
    }
    
    /**
     * Palauttaa säilötyn tilan.
     * @return säilötty tila
     */
    public State getState() {
        return state;
    }
    /**
     * Asettaa säilöttävän tilan
     * @param state säilöttävä tila
     */
    public void setState(State state) {
        this.state = state;
    }
    
    /**
     * Palauttaa kuljetun polun pituuden.
     * @return polun pituus
     */
    public int getPathLength() {
        return pathLength;
    }
    
    /**
     * Kasvattaa polun pituutta yhdellä.
     */
    public void incPathLength() {
        pathLength++;
    }
    
    /**
     * Palauttaa viimeisimmän käyntilaskurin lukeman. Jos säiliöllä ei ole
     * käyntilaskureita, palauttaa nollan.
     * @return viimeisimmän käyntilaskurin lukeman tai 0, jos käyntilaskureita ei ole.
     */
    public int getCounter() {
        if (top < 0) {
            return 0;
        } else {
            return counters[top];
        }
    }
    
    /**
     * Kasvattaa viimeisintä käyntilaskuria yhdellä.
     */
    public void increaseCounter() {
        if (top >= 0) {
            counters[top]++;
        }
    }
    
    /**
     * Lisää laskuritaulukkoon uuden käyntilaskurin, jonka alkuarvo on 0.
     */
    public void addCounter() {
        if (top < counters.length - 1) {
            top++;
            counters[top] = 0;
        } else {
            System.out.println("Counter overflow");
        }
    }
    /**
     * Poistaa viimeisimmän käyntilaskurin.
     */
    public void removeCounter() {
        top--;
    }
    
    /**Ilmaisee, onko säiliöllä käyntilaskureita*/
    public boolean hasCounters() {
        return top >= 0;
    }

    
    
}
