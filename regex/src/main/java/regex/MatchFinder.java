package regex;

import regex.io.LineReader;
import java.util.ArrayList;
import java.util.List;
import regex.domain.Match;
import regex.matcher.RegexMatcher;

/**
 * MatchFinder etsii ja tallentaa säännöllisen lausekkeen toteuttavia
 * merkkijonoja.
 */
public class MatchFinder {

    /**
     * Säännöllisten lauseiden tulkki
     */
    private RegexMatcher matcher;
    /**
     * Lista löydetyistä merkkijonoista Match-olioina
     *
     * @see Match
     */
    private List<Match> matches;
    /**
     * Tiedoston lukija
     *
     * @see LineReader
     */
    private LineReader reader;

    /**
     * Konstruktori saa parametreina käytettävän RegexMatcher-rajapinnan toteuttavan
     * säännöllisten lausekkeiden tulkin ja tiedoston lukijan.
     *
     * @param matcher säännöllisten lausekkeiden tulkki
     * @param reader tiedoston lukija
     * @see RegexMatcher
     */
    public MatchFinder(RegexMatcher matcher, LineReader reader) {
        this.matches = new ArrayList<Match>();
        this.matcher = matcher;
        this.reader = reader;
    }

    /**
     * Lukee tiedostoa rivi kerrallaan ja etsii riviltä säännöllisen lausekkeen
     * toteuttavia merkkijonoja. Palauttaa lukijan tiedoston alkuun, kun kaikki
     * rivit on luettu.
     */
    public void read() {

        String line = reader.readLine();
        while (line != null) {
            findMatchesInLine(line, reader.getLineNumber());
            line = reader.readLine();
        }
        reader.reset();
    }

    /**
     * Käy rivin läpi merkki kerrallaan. Etsii riviltä säännöllisen lausekkeen
     * toteuttavia merkkijonoja ja tallentaa ne Match-oloina.
     *
     * @param line rivi
     * @param lineNumber rivin numero
     * @see Match
     */
    private void findMatchesInLine(String line, int lineNumber) {
        int start = 0;
        while (start < line.length()) {
            if (matcher.find(start, line)) {
                int a = matcher.start();
                int b = matcher.end();
                if (b - a <= 0) {
                    start++;
                    continue;
                }
                String lineToSave = line;
                if (lineToSave.length() > 100) {
                    lineToSave = line.substring(a, b);
                }
                matches.add(new Match(lineNumber, a, b - 1, lineToSave));
                start = b;
            } else {
                start = line.length();
            }
        }
    }

    /**
     * Palauttaa löydetyt merkkijonot listana Match-olioita
     *
     * @see Match
     */
    public List<Match> getMatches() {
        return matches;
    }

    /**
     * Tulostaa löydetyt merkkijonot tai "No matches" jos merkkijonoja ei ole löytynyt.
     */
    public void print() {
        if (matches.isEmpty()) {
            System.out.println("No matches");
            return;
        }
        System.out.println("Found " + matches.size() + " matches in " + reader.getFile());
        for (Match m : matches) {
            System.out.println(m.toString());
        }
    }
    
    /**
     * Antaa tulkille uuden säännöllisen lausekkeen. Tyhjentää edellisten hakutulosten listan.
     * @param regex säännöllinen lauseke
     */
    public void setRegex(String regex) {
        matcher.setRegex(regex);
        reader.reset();
        matches = new ArrayList<Match>();
    }
}
