
package regex.domain;

/**
 * Luokka kuvaa kaaren, joka hyväksyy 8-bittisistä merkeistä muodostuvia merkkijoukkoja
 * tai merkkivälin.
 * @author heidi
 */
public class CharGroupEdge implements Edge {
    
    /**Taulukko, johon kaaren hyväksymät merkit tallennetaan*/
    private byte accepted[];
    /**Kaaren kohdetila*/
    private State target;
    /**Ilmaisee, onko kaari osa laskuritilaan liittyvää silmukkaa*/
    private boolean counting;
    
    /**
     * Konstruktori sa parametrina kaaren kohdetilan.
     * @param target kohdetila
     */
    public CharGroupEdge(State target) {
        this.target = target;
        this.accepted = new byte[256];
        this.counting = false;

    }
    
    public CharGroupEdge() {
        this(null);
    }
    /**
     * Lisää hyväksyttyjen merkkien taulukkoon kaikki merkit annetulta väliltä 
     * mukaanlukien välin alun ja lopun.
     * @param start välin alku
     * @param stop välin loppu
     */
    public void addRange(char start, char stop) {
        int a = (int) start;
        int b = (int) stop;
        for (int i = a; i <= b; i++) {
            accepted[i] = (byte)1;
        }
    }
    
    /**
     * Lisää hyväksyttyjen merkkien taulukkoon yksittäisen merkin c.
     * @param c hyväksyttävä merkki
     */
    public void addChar(char c) {
        int i = (int) c;
        accepted[i] = (byte)1;
    }
    
    /**
     * Palauttaa tyhjän merkin.
     * @return ' '
     */
    @Override
    public char getC() {
        return ' ';
    }

    @Override
    public State getTarget() {
        return target;
    }

    @Override
    public void setTarget(State target) {
        this.target = target;
    }
    /**
     * Tarkistaa, onko merkki hyväksyttyjen merkkien taulukossa.
     * @param c tarkistettava merkki
     * @return true, jos merkki on taulukossa, muulloin false
     */
    @Override
    public boolean matches(char c) {
        int i = (int) c;
        return (int) accepted[i] == 1;
    }
    
    @Override
    public void makeCounting() {
        this.counting = true;
    }

    @Override
    public boolean isCounting() {
        return this.counting;
    }

}
