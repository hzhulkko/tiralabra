
package regex.utils;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class ListTest {
    
    private List<Integer> list;
    
    @Before
    public void setUp() {
        list = new List<Integer>(5); 

    }
    
    @Test
    public void newListIsEmpty() {
        assertTrue(list.isEmpty() && list.size() == 0);
    }
    
    @Test
    public void addIncreasesSize() {
        for (int i = 0; i < 5; i++) {
            list.add(i);
        }
        assertEquals(5, list.size());
    }
    
    @Test
    public void addingOverInitialCapacitySucceeds() {
        for (int i = 0; i < 10; i++) {
            list.add(i);
        }
        assertEquals(10, list.size());
    }
    
    @Test
    public void getReturnsListElement() {
        list.add(1);
        int k = list.get(0);
        assertEquals(k, 1);
    }
    
    @Test
    public void clearMakesEmptylist() {
        for (int i = 0; i < 5; i++) {
            list.add(i);
        }
        list.clear();
        assertTrue(list.isEmpty());
    }
}
