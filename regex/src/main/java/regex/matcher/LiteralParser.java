
package regex.matcher;

import regex.io.StringReader;
import regex.domain.AnyCharEdge;
import regex.domain.DefaultEdge;
import regex.domain.State;

/**
 * Luokka toteuttaa säännöllisen lausekkeen jäsentelijän, joka palauttaa perustilan sekä
 * yhtä merkkiä tai mitä tahansa merkkiä vastaavan kaaren.
 */

public class LiteralParser implements Parser {
    
    private State state;
    private StringReader reader;
    
    public LiteralParser(StringReader reader) {
        this.state = new State(1);
        this.reader = reader;

        
    }

    @Override
    public void parse(char c) {
        switch(c) {
            case '.':
                state.add(new AnyCharEdge());
                break;
            default:
                state.add(new DefaultEdge(c));
                break;
        }
    }

    @Override
    public Fragment getFragment() {
        State s = state.copy();
        return new Fragment(s);
    }

    @Override
    public void read() {
        if (reader.hasChar()) {
            parse(reader.getChar());
        }
    }
    
}
