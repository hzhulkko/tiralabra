/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package regex;

import regex.io.LineReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import org.junit.*;
import org.junit.rules.TemporaryFolder;
import regex.matcher.RegexMatcher;
import regex.matcher.StandardMatcher;
import static org.junit.Assert.*;
import regex.domain.Match;
import regex.matcher.CustomMatcher;

public class MatchFinderTest {

    private MatchFinder finder;
    private File testFile;
    private RegexMatcher matcher;
    
    @Rule
    public TemporaryFolder tmp = new TemporaryFolder();
    
    @Before
    public void setUp() throws IOException {
        testFile = tmp.newFile("test.txt");
        matcher = new CustomMatcher();
        LineReader r = new LineReader(testFile);
        finder = new MatchFinder(matcher, r);
    }
    
    @Test
    public void readingEmptyFileReturnsEmptylist() {
       finder.read();
       List<Match> list = finder.getMatches();
       assertEquals(0, list.size());
       
    }

    @Test
    public void readingUnmatchingStringReturnsEmptyFile() {
        matcher.setRegex("ab+c");
        write("acccbaaacb");
        finder.read();
        List<Match> list = finder.getMatches();
        assertEquals(0, list.size());
    }
    
    @Test
    public void readingMatchingStringReturnsUnemptyList() {
        matcher.setRegex("ab*c");
        write("acccbaaacb");
        finder.read();
        List<Match> list = finder.getMatches();
        assertEquals(2, list.size());
    }
    
    @Test
    public void readingMatchingStringReturnsUnemptyList2() {
        matcher.setRegex("ab*c");
        write("acccbaaacb\nacccbaaacb");
        finder.read();
        List<Match> list = finder.getMatches();
        assertEquals(4, list.size());
    }
    
    /*@Test
    public void readingMatchingStringReturnsUnemptyList3() {
        matcher.setRegex("tu{1,2}li");
        write("tuuli tuuli kohtuiseksi\ntuli");
        finder.read();
        List<Match> list = finder.getMatches();
        StringBuilder sb = new StringBuilder();
        for (Match m : list) {
            sb.append(m.toString());
            sb.append("\n");
        }
        String exp = "Line 1 indeces 0...4: tuuli\nLine 1 indeces 6...10: tuuli\nLine 2 indeces 0...3: tuli\n";
        assertEquals(exp, sb.toString());
    }*/
    
    @Test
    public void readingFindsDifferentRegexes() {
        write("acbacb\nacbacb");
        matcher.setRegex("ba");
        finder.read();
        matcher.setRegex("ac");
        finder.read();
        List<Match> list = finder.getMatches();
        assertEquals(6, list.size());      
    }
       
    private void write(String text) {
        FileWriter writer;
        try {
            writer = new FileWriter(testFile);
            writer.append(text);
            writer.close();
        } catch (IOException ex) {
        }
        
    }
    
}
