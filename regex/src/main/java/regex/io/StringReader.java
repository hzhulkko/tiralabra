/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package regex.io;

/**
 * Luokka toteuttaa merkkijonon lukijan, joka lukee merkkijonoa merkki kerrallaan ja 
 * palauttaa lukuvuorossa olevan merkin. Sama lukija voidaan jakaa useamman 
 * sitä käyttävän luokan kesken, jolloin kaikki lukijaa käyttävät luokat pysyvät samassa 
 * kohdassa merkkijonon luentaa.
 * 
 * MainParser ja Parser-luokan apujäsentelijät jakavat yhteisen merkkijonolukijan
 * säännöllisen lausekkeen luennan aikana.
 *
 */
public class StringReader {
    
    /**Luettava merkkijono*/
    private String regex;
    /**Luettava indeksi*/
    private int index;
    
    /**
     * Konstruktori saa parametrina luettavan merkkijono ja aloittaa lukemisen indeksistä 0.
     * @param regex luettava merkkijono
     */
    public StringReader(String regex) {
        this.regex = regex;
        this.index = 0;
    }
    
    /**Palauttaa luettavan merkkijonon*/
    public String getRegex() {
        return this.regex;
    }
    /**Palauttaa luentavuorossa olevan merkin*/
    public char getChar() {
        char c = regex.charAt(index);
        index++;
        return c;
        
    }
    /**
     * Ilmaisee, onko luettavia merkkejä vielä jäljellä.
     * @return true, jos luettavaa on jäljellä, false jos lukija on päässyt
     * merkkijonon loppuu.
     */
    public boolean hasChar() {
        return index < regex.length();
    }
        
}
