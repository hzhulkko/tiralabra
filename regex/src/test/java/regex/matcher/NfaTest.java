/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package regex.matcher;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class NfaTest {
    
    private Nfa nfa;
   
    
    @Before
    public void setUp() {
        nfa = new Nfa();
    }
    
    @Test(expected = IllegalStateException.class)
    public void uncompiledNfaThrowsException() {
        nfa.match("abc");
    }
    
    @Test
    public void matchLiterals1() {
        nfa.compile("abc");
        assertTrue(nfa.match("abc"));
    }
    
    @Test
    public void matchLiterals2A() {
        nfa.compile("abc");
        assertTrue(nfa.match("abc"));
    }
    
    @Test
    public void matchLiterals2B() {
        nfa.compile("abc");
        assertFalse(nfa.match("ab"));
    }
    
    @Test
    public void matchLiterals2C() {
        nfa.compile("abc");
        assertFalse(nfa.match("aba"));
    }
    
    
    @Test
    public void matchLiterals3A() {
        nfa.compile("abc");
        assertTrue(nfa.match("abaabc"));
    }
    
    @Test
    public void matchLiterals3B() {
        nfa.compile("abc");
        assertTrue(nfa.match("abaabcbc"));
    }
    
    @Test
    public void matchZeroOrMore1A() {
        nfa.compile("ab*c");
        assertTrue(nfa.match("ac"));
    }
    
    @Test
    public void matchZeroOrMore1B() {
        nfa.compile("ab*c");
        assertTrue(nfa.match("abc"));
    }
    
    @Test
    public void matchZeroOrMore1C() {
        nfa.compile("ab*c");
        assertTrue( nfa.match("abbbbc"));
    }
    
    @Test
    public void matchZeroOrMore2A() {
        nfa.compile("ab*c");
        assertTrue(nfa.match("ddacff"));
    }
    
    @Test
    public void matchZeroOrMore2B() {
        nfa.compile("ab*c");
        assertTrue(nfa.match("dfabcdf"));
    }
    
    @Test
    public void matchZeroOrMore2C() {
        nfa.compile("ab*c");
        assertTrue(nfa.match("fdabbbbcfd"));
    }
    
    @Test
    public void matchZeroOrMore3A() {
        nfa.compile("a(bc)*d");
        assertTrue(nfa.match("ad"));
    }
    
    @Test
    public void matchZeroOrMore3B() {
        nfa.compile("a(bc)*d");
        assertTrue(nfa.match("abcd"));
    }
    
    @Test
    public void matchZeroOrMore3C() {
        nfa.compile("a(bc)*d");
        assertTrue(nfa.match("abcbcd"));
    }
    
    @Test
    public void matchZeroOrMore4A() {
        nfa.compile("a(b*c)d");
        assertTrue(nfa.match("acd"));
    }
    
    @Test
    public void matchZeroOrMore4B() {
        nfa.compile("a(b*c)d");
        assertTrue(nfa.match("abcd"));
    }
    
    @Test
    public void matchZeroOrMore4C() {
        nfa.compile("a(b*c)d");
        assertTrue(nfa.match("abbbbcd"));
    }
    
    @Test
    public void matchZeroOrMore5A() {
        nfa.compile("a(bc*)d");
        assertTrue(nfa.match("abd"));
    }
    
    @Test
    public void matchZeroOrMore5B() {
        nfa.compile("a(bc*)d");
        assertTrue(nfa.match("abcd"));
    }
    
    @Test
    public void matchZeroOrMore5C() {
        nfa.compile("a(bc*)d");
        assertTrue(nfa.match("abccccd"));
    }
    
    @Test
    public void matchZeroOrMore6A() {
        nfa.compile("a(bc)*d");
        assertFalse(nfa.match("abd"));
    }
    
    @Test
    public void matchZeroOrMore6B() {
        nfa.compile("a(bc)*d");
        assertTrue(nfa.match("abcd"));
    }
    
    @Test
    public void matchZeroOrMore6C() {
        nfa.compile("a(bc)*d");
        assertFalse(nfa.match("abcbccd"));
    }
    
    @Test
    public void matchZeroOrMore7A() {
        nfa.compile("ab*c");
        assertTrue(nfa.match("abc") && nfa.match("ababc"));
    }
    
    @Test
    public void matchZeroOrMore7B() {
        nfa.compile("ab*c");
        assertTrue(nfa.match("ababc"));
    }
    
    @Test
    public void matchOneOrMore1A() {
        nfa.compile("ab+c");
        assertTrue(nfa.match("abc"));
    }
    
    @Test
    public void matchOneOrMore1B() {
        nfa.compile("ab+c");
        assertTrue(nfa.match("abbbbbc"));
    }
    
    @Test
    public void matchOneOrMore1C() {
        nfa.compile("ab+c");
        assertFalse(nfa.match("ac"));
    }
    
    @Test
    public void matchOneOrMore2A() {
        nfa.compile("ab+c");
        assertTrue(!nfa.match("ac"));
    }
    
    @Test
    public void matchOneOrMore2B() {
        nfa.compile("ab+c");
        assertFalse(nfa.match("abbdbbc"));
    }
    
    @Test
    public void matchOneOrMore3A() {
        nfa.compile("(ab)+c");
        assertTrue(nfa.match("abc"));
    }
    
    @Test
    public void matchOneOrMore3B() {
        nfa.compile("(ab)+c");
        assertTrue(nfa.match("abababc"));
    }
    
    @Test
    public void matchOneOrMore3C() {
        nfa.compile("(ab)+c");
        assertFalse(nfa.match("c"));
    }
    
    @Test
    public void matchOneOrMore4A() {
        nfa.compile("(a(b+c)d");
        assertTrue(nfa.match("abcd"));
    }
    
    @Test
    public void matchOneOrMore4B() {
        nfa.compile("(a(b+c)d");
        assertTrue(nfa.match("abbbbcd"));
    }
    
    @Test
    public void matchOneOrMore4C() {
        nfa.compile("(a(b+c)d");
        assertFalse(nfa.match("acd"));
    }
    
    @Test
    public void matchZeroOrOne1A() {
        nfa.compile("ab?c");
        assertTrue(nfa.match("ac"));
    }
    
    @Test
    public void matchZeroOrOne1B() {
        nfa.compile("ab?c");
        assertTrue(nfa.match("abc"));
    }
    
    @Test
    public void matchZeroOrOne1C() {
        nfa.compile("ab?c");
        assertFalse(nfa.match("abbc"));
    }
    
    @Test
    public void matchZeroOrOne2A() {
        nfa.compile("(ab)?c");
        assertTrue(nfa.match("c"));
    }
    
    @Test
    public void matchZeroOrOne2B() {
        nfa.compile("(ab)?c");
        assertTrue(nfa.match("abc"));
    }
    
    @Test
    public void matchZeroOrOne2C() {
        nfa.compile("(ab)?c");
        assertTrue(nfa.match("ababc"));
    }
    
    @Test
    public void matchZeroOrOne3A() {
        nfa.compile("d(ab)?c");
        assertTrue(nfa.match("dc"));
    }
    
    @Test
    public void matchZeroOrOne3B() {
        nfa.compile("d(ab)?c");
        assertTrue(nfa.match("dabc"));
    }
    
    @Test
    public void matchZeroOrOne3C() {
        nfa.compile("d(ab)?c");
        assertFalse(nfa.match("dababc"));
    }
    
    @Test
    public void matchZeroOrOne4A() {
        nfa.compile("d(a?)c");
        assertTrue(nfa.match("dc"));
    }
    
    @Test
    public void matchZeroOrOne4B() {
        nfa.compile("d(a?)c");
        assertTrue(nfa.match("dac"));
    }
    
    @Test
    public void matchZeroOrOne4C() {
        nfa.compile("d(a?)c");
        assertFalse(nfa.match("daac"));
    }
    
    @Test
    public void matchAlternatives1A() {
        nfa.compile("a|b|c");
        assertTrue(nfa.match("a"));
    }
    
    @Test
    public void matchAlternatives1B() {
        nfa.compile("a|b|c");
        assertTrue(nfa.match("b"));
    }
    
    @Test
    public void matchAlternatives1C() {
        nfa.compile("a|b|c");
        assertTrue(nfa.match("c"));
    }

    @Test
    public void matchAlternatives2A() {
        nfa.compile("a(b|c)");
        assertTrue(nfa.match("ab"));
    }
    
    @Test
    public void matchAlternatives2B() {
        nfa.compile("a(b|c)");
        assertTrue(nfa.match("ac"));
    }
    
    @Test
    public void matchAlternatives2C() {
        nfa.compile("a(b|c)");
        assertFalse(nfa.match("a"));
    }
    
    @Test
    public void matchAlternatives3A() {
        nfa.compile("a(b|c)d");
        assertTrue(nfa.match("abd"));
    }
    
    @Test
    public void matchAlternatives3B() {
        nfa.compile("a(b|c)d");
        assertTrue(nfa.match("acd"));
    }
    
    @Test
    public void matchAlternatives3C() {
        nfa.compile("a(b|c)d");
        assertFalse(nfa.match("ad"));
    }
    
    @Test
    public void matchAlternatives4A() {
        nfa.compile("(a|b)(c|d)");
        assertTrue(nfa.match("ad"));
    }
    
    @Test
    public void matchAlternatives4B() {
        nfa.compile("(a|b)(c|d)");
        assertTrue(nfa.match("ac"));
    }
    
    @Test
    public void matchAlternatives4C() {
        nfa.compile("(a|b)(c|d)");
        assertTrue(nfa.match("bd"));
    }
    
    @Test
    public void matchAlternatives4D() {
        nfa.compile("(a|b)(c|d)");
        assertTrue(nfa.match("bc"));
    }
    
    @Test
    public void matchAlternatives5A() {
        nfa.compile("ab|cd");
        assertTrue(nfa.match("ab"));
    }
    
    @Test
    public void matchAlternatives5B() {
        nfa.compile("ab|cd");
        assertTrue(nfa.match("cd"));
    }
    
    @Test
    public void matchAlternatives5C() {
        nfa.compile("ab|cd");
        assertFalse(nfa.match("ad"));
    }
    
    @Test
    public void matchAlternatives6A() {
        nfa.compile("ab|cdefg");
        assertTrue(nfa.match("ab"));
    }
    
    @Test
    public void matchAlternatives6B() {
        nfa.compile("ab|cdefg");
        assertTrue(nfa.match("cdefg"));
    }
    
    @Test
    public void matchAlternatives6C() {
        nfa.compile("ab|cdefg");
        assertFalse(nfa.match("adfg"));
    }
    
    @Test
    public void matchAlternatives7A() {
        nfa.compile("ab|cdefg|(hij|k)");
        assertTrue(nfa.match("ab"));
    }
    
    @Test
    public void matchAlternatives7B() {
        nfa.compile("ab|cdefg|(hij|k)");
        assertTrue(nfa.match("cdefg"));
    }
    
    @Test
    public void matchAlternatives7C() {
        nfa.compile("ab|cdefg|(hij|k)");
        assertTrue(nfa.match("k"));
    }
    
    @Test
    public void matchAnyChar1A() {
        nfa.compile("a.b");
        assertTrue(nfa.match("adb"));
    }
    
    @Test
    public void matchAnyChar1B() {
        nfa.compile("a.b");
        assertTrue(nfa.match("a@b"));
    }
    
    @Test
    public void matchAnyChar1C() {
        nfa.compile("a.b");
        assertFalse(nfa.match("ab"));
    }
    
    @Test
    public void matchGroup1A() {
        nfa.compile("[bc]");
        assertTrue(nfa.match("b"));
    }
    
    @Test
    public void matchGroup1B() {
        nfa.compile("[bc]");
        assertTrue(nfa.match("c"));
    }
    
    @Test
    public void matchGroup1C() {
        nfa.compile("[bc]");
        assertFalse(nfa.match("a"));
    }
    
    @Test
    public void matchGroup2A() {
        nfa.compile("a[bc]d");
        assertTrue(nfa.match("abd"));
    }
    
    @Test
    public void matchGroup2B() {
        nfa.compile("a[bc]d");
        assertTrue(nfa.match("acd"));
    }
    
    @Test
    public void matchGroup2C() {
        nfa.compile("a[bc]d");
        assertFalse(nfa.match("ad"));
    }
    
    @Test
    public void matchGroup3A() {
        nfa.compile("a[bc]*");
        assertTrue(nfa.match("a"));
    }
    
    @Test
    public void matchGroup3B() {
        nfa.compile("a[bc]*");
        assertTrue( nfa.match("accccc"));
    }
    
    @Test
    public void matchGroup3C() {
        nfa.compile("a[bc]*");
        assertTrue(nfa.match("abbbbb"));
    }
    
    @Test
    public void matchGroup4A() {
        nfa.compile("a[bc]+");
        assertFalse(nfa.match("a"));
    }
    
    @Test
    public void matchGroup4B() {
        nfa.compile("a[bc]+");
        assertTrue(nfa.match("accccc"));
    }
    
    @Test
    public void matchGroup4C() {
        nfa.compile("a[bc]+");
        assertTrue(nfa.match("abbbbb"));
    }
    
    @Test
    public void matchGroup5A() {
        nfa.compile("a[bc-]");
        assertFalse(nfa.match("a"));
    }
    
    @Test
    public void matchGroup5B() {
        nfa.compile("a[bc-]");
        assertTrue(nfa.match("a-"));
    }
    
    @Test
    public void matchGroup6A() {
        nfa.compile("a[-bc]");
        assertFalse(nfa.match("a"));
    }
    
    @Test
    public void matchGroup6B() {
        nfa.compile("a[-bc]");
        assertTrue(nfa.match("a-"));
    }
    
    @Test
    public void matchRange1A() {
        nfa.compile("[a-d]");
        assertTrue(nfa.match("a"));
    }
    
    @Test
    public void matchRange1B() {
        nfa.compile("[a-d]");
        assertTrue(nfa.match("b"));
    }
    
    @Test
    public void matchRange1C() {
        nfa.compile("[a-d]");
        assertTrue(nfa.match("c"));
    }
    
    @Test
    public void matchRange1D() {
        nfa.compile("[a-d]");
        assertTrue(nfa.match("d"));
    }
      
    @Test
    public void matchRange2A() {
        nfa.compile("f[a-d]f");
        assertTrue(nfa.match("faf"));
    }
    
    @Test
    public void matchRange2B() {
        nfa.compile("f[a-d]f");
        assertTrue(nfa.match("fbf"));
    }
    
    @Test
    public void matchRange2C() {
        nfa.compile("f[a-d]f");
        assertTrue(nfa.match("fcf"));

    }
    
    @Test
    public void matchRange2D() {
        nfa.compile("f[a-d]f");
        assertTrue(nfa.match("fdf"));
        assertFalse(nfa.match("ff"));
    }
    
    @Test
    public void matchRange2E() {
        nfa.compile("f[a-d]f");
        assertFalse(nfa.match("ff"));
    }
    
    @Test
    public void matchRange3() {
        nfa.compile("[A-Da-d]");
        assertTrue(nfa.match("a"));
    }
    
    @Test
    public void matchRange3A() {
        nfa.compile("[A-Da-d]");
        assertTrue(nfa.match("b"));
    }
    
    @Test
    public void matchRange3B() {
        nfa.compile("[A-Da-d]");
        assertTrue( nfa.match("c"));
    }
    
    @Test
    public void matchRange3C() {
        nfa.compile("[A-Da-d]");
        assertTrue(nfa.match("d"));
    }
    
    @Test
    public void matchRange3D() {
        nfa.compile("[A-Da-d]");
        assertTrue(nfa.match("A"));
    }
    
    @Test
    public void matchRange3E() {
        nfa.compile("[A-Da-d]");
        assertTrue(nfa.match("B"));
    }
    
    @Test
    public void matchRange3F() {
        nfa.compile("[A-Da-d]");
        assertTrue(nfa.match("C"));
    }
    
    @Test
    public void matchRange3G() {
        nfa.compile("[A-Da-d]");
        assertTrue(nfa.match("D"));
    }
    
    @Test
    public void matchExactQuantitiesA() {
        nfa.compile("ab{1,3}c");
        assertFalse(nfa.match("a"));
    }
    
    @Test
    public void matchExactQuantitiesB() {
        nfa.compile("ab{1,3}c");
        assertTrue(nfa.match("abc"));
    }
    
    @Test
    public void matchExactQuantities1() {
        nfa.compile("ab{1,3}c");
        assertTrue(nfa.match("ababbc"));
    }
    
    @Test
    public void matchExactQuantities2() {
        nfa.compile("a{1,3}b{3,3}");
        assertFalse(nfa.match("ab"));
    }
        
    @Test
    public void matchExactQuantities3() {
        nfa.compile("a(bc|def){1,3}");
        assertTrue(nfa.match("abcbc"));
    }
    
    @Test
    public void matchExactQuantities4() {
        nfa.compile("a(bc|def){3}");
        assertTrue(nfa.match("abcdefbc"));
    }
    
    @Test
    public void matchExactQuantities5() {
        nfa.compile("ab{2}c");
        assertTrue(nfa.match("abbc"));
    }
    
    @Test
    public void matchExactQuantities6() {
        nfa.compile("ab{2}c");
        assertFalse(nfa.match("abc"));
    }
    
    @Test
    public void matchExactQuantities7() {
        nfa.compile("ab{12}");
        assertTrue(nfa.match("abbbbbbbbbbbb"));
    }
    
    @Test
    public void matchExactQuantities8() {
        nfa.compile("ab{12}");
        assertFalse(nfa.match("ab"));
    }
    
    @Test
    public void matchExactQuantities9() {
        nfa.compile("a{1,3}b{3,3}");
        assertTrue(nfa.match("aaabbb"));
    }
    
    @Test
    public void matchExactQuantities10() {
        nfa.compile("ab{1,12}");
        assertTrue(nfa.match("abbb") && nfa.match("abbbbbbbbbbbb"));
    }
    
    @Test
    public void matchExactQuantities11() {
        nfa.compile("ab{12}");
        assertTrue(nfa.match("abbbbbabbbbbbbbbbbb"));
    }
    
    @Test
    public void matchExactQuantities12() {
        nfa.compile("ab{15}");
        assertTrue(nfa.match("abbbcabbaababbbbbbbbbabbbbbabbbbbbbbbbbbbbb"));
    }
    
    @Test
    public void matchDifficult1A() {
        nfa.compile("(a(bc|df)+)*g");
        assertTrue(nfa.match("g"));
    }
    
    @Test
    public void matchDifficult1B() {
        nfa.compile("(a(bc|df)+)*g");
        assertTrue(nfa.match("abcg"));
    }
    
    @Test
    public void matchDifficult1C() {
        nfa.compile("(a(bc|df)+)*g");
        assertTrue(nfa.match("adfg"));
    }
    
    @Test
    public void matchDifficult1D() {
        nfa.compile("(a(bc|df)+)*g");
        assertTrue(nfa.match("zzzg"));
    }
    
    @Test
    public void matchDifficult2A() {
        nfa.compile("c(b(a+)*)+");
        assertFalse(nfa.match("c"));
    }
    
    @Test
    public void matchDifficult2B() {
        nfa.compile("c(b(a+)*)+");
        assertTrue(nfa.match("cb"));
    }
    
    @Test
    public void matchDifficult2C() {
        nfa.compile("c(b(a+)*)+");
        assertTrue(nfa.match("cba"));
    }
    
    @Test
    public void matchDifficult2D() {
        nfa.compile("c(b(a+)*)+");
        assertTrue(nfa.match("cbaaa"));
    }
    
    @Test
    public void matchDifficult3() {
        nfa.compile("(a?)*a*");
        assertTrue(nfa.match("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!"));
    }
    
    @Test
    public void matchDifficult4() {
        nfa.compile("(a+)+");
        assertTrue(nfa.match("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!"));
    }
    
    @Test
    public void matchDifficult5() {
        nfa.compile("([a-zA-Z]+)*");
        assertTrue(nfa.match("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!"));
    }
    
    @Test
    public void matchDifficult6() {
        nfa.compile("(a|aa)+");
        assertTrue(nfa.match("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!"));
    }
    
    @Test
    public void matchDifficult7() {
        nfa.compile("(a|a?)+");
        assertTrue(nfa.match("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!"));
    }
    
    /*@Test
    public void matchDifficult8() {
        nfa.compile("(.*a){20}");
        assertTrue(nfa.match("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!"));
    }*/
    
    @Test
    public void matchDifficult10() {
        nfa.compile("TT[GA].{3,3}T.{16,18}[TC].TAA[TAC]");
        assertTrue(nfa.match("TGTTAAATCGGCCATAACTGGCTGTATTACTGTTATTATTGCTGTCATAAGAGCGATACTGACTTAAATT"));
    }
    
    
    
}
