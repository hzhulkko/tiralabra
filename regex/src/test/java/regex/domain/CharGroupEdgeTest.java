/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package regex.domain;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import regex.domain.CharGroupEdge;

/**
 *
 * @author heidi
 */
public class CharGroupEdgeTest {
    
    CharGroupEdge edge;
    
    @Before
    public void setUp() {
        edge = new CharGroupEdge();
    }
    
    @Test
    public void newEdgeMatchesNone() {
        assertFalse(edge.matches('a'));
        assertFalse(edge.matches('6'));
        assertFalse(edge.matches('@'));
    }
    
    @Test
    public void edgeMatchesAddedChars() {
        edge.addChar('a');
        edge.addChar('£');
        
        assertTrue(edge.matches('a') && edge.matches('£') && !edge.matches('A'));
    }
    
    @Test
    public void edgeMatchesAddedRange() {
        edge.addRange('a', 'd');
        assertTrue(edge.matches('a') && edge.matches('b') && edge.matches('c') && edge.matches('d'));
    }
    
}
