
package regex.matcher;

import regex.io.StringReader;
import regex.domain.CharGroupEdge;
import regex.domain.State;

/**Luokka toteuttaa säännöllisen lausekkeen jäsentelijän, joka palauttaa useaa merkkiä
 * tai merkkijoukkoa vastaavan tila-automaation osan. jäsentelijä lukee ja tulkistee
 * hakasulkeilla merkittyä säännöllisen lausekkeen osaa. Merkki ']' päättää luennan.
 * Esimerkkejä jäsentelijän tulkitsemista lausekkeista:
 * 
 * abc] = a tai b tai c
 * A-Z] = merkit välillä A:sta Z:aan
 * ab-] = a tai b tai -
 */

public class GroupParser implements Parser {
    /**Ilmaisee, onko luenta päättynyt*/
    private boolean quit;
    /**Ilmaisee, onko kyseessä merkkijoukko*/
    private boolean isRange;
    /**Pitää tallessa viimeisintä lisättyä merkkiä*/
    private char lastChar;
    /**Merkkijoukkoa vastaava kaari*/
    private CharGroupEdge edge;
    /**Säännöllisen lausekkeen lukija*/
    private StringReader reader;
    
    public GroupParser(StringReader reader) {
        this.reader = reader;
        this.quit = false;
        this.isRange = false;
        this.edge = new CharGroupEdge();
        this.lastChar = ' ';
    }
    
    @Override
    public void read() {
        while (!quit && reader.hasChar()) {
            parse(reader.getChar());
        }
    }

    @Override
    public void parse(char c) {
        switch(c) {
            case ']':
                quit = true;
            case '-':
                isRange = true;
                break;
            default:
                if (isRange && lastChar != ' ') {
                    edge.addRange(lastChar, c);
                    isRange = false;
                } else if (isRange && lastChar == ' ') {
                    edge.addChar('-');
                    isRange = false;
                } else {
                    edge.addChar(c);
                }
                lastChar = c;
                
        }
    }

    @Override
    public Fragment getFragment() {
        quit = true;
        if (isRange) {
            edge.addChar('-');
        }
        State state = new State(1);
        state.add(edge);
        return new Fragment(state);

        
    }

    
}
