
package regex.matcher;

import regex.domain.State;
import regex.domain.Edge;
import regex.utils.List;
import regex.utils.Queue;

/**Luokka toteuttaa tila-automaatin lukijan, joka etenee tila-automaatissa luetun 
 * merkin perusteella ja tallentaa säännölliseen lausekkeen toteuttavan merkkijono sijainnin.*/  

public class NfaMatcher {
       
    /**Käsiteltävien tilojen jono*/
    private Queue<StateContainer> current;
    /**Seuraavaksi käsiteltävien tilojen jono*/
    private Queue<StateContainer> next;
    /**Lista automaatin kaikista tiloista*/
    private List<State> allStates;
    /**Tila-automaatin alkutila*/
    private State starting;
    /**Tila-automaatin lopputila*/
    private State terminal;
    /**Ilmaisee, onko päästy lopputilaan*/
    private boolean endFound;
    /**Ilmaisee, voiko tila-automaatissa edetä seuraaviin tiloihin*/
    private boolean deadEnd;
    /**Sännöllisen lausekkeen toteuttavan merkkijonon loppu*/
    private int end;
    /**Säännöllisen lausekkeen toteuttavan merkkijonon alku*/
    private int start;
    /**Kuinka monta laskevaa tilaa automaatissa on korkeintaan*/
    private int maxCounters;
    /**Muuttujan avulla varmistetaan, ettei samaa tilaa lisätä kahteen kertaa samalle listalle*/
    private int listGenerationNumber;
    /**
     * Konstruktori saa parametrina tila-automaatin alkutilan, lopputilan ja listan
     * tila-automaatin sisältävistä käyntikertoja laskevista tiloista
     * @param starting alkutila
     * @param terminal lopputila
     * @param counting laskevien tilojen lukumäärä
     */
    public NfaMatcher(State starting, State terminal, List<State> allStates, int counting) {
        this.starting = starting;
        this.terminal = terminal;
        this.allStates = allStates;
        
        this.current = new Queue<StateContainer>();
        this.next = new Queue<StateContainer>(); 
        
        this.end = 0;
        this.start = Integer.MAX_VALUE-1;
        this.endFound = false;
        this.deadEnd = true;
        this.maxCounters = counting + 2;

    }

    /**
     * Palauttaa edellisen säännöllisen lausekkeen toteuttavan merkkijonon
     * loppuindeksiä seuraavan indeksin.
     *
     * @return loppuindeksi + 1
     */
    public int end() {
        return end;
    }

    /**
     * Palauttaa edellisen säännöllisen lausekkeen toteuttavan merkkijonon
     * alkuindeksin.
     *
     * @return alkuindeksi
     */
    public int start() {
        return start;
    }
    
    /**Asettaa automaatin alkuperäiseen lähtötilaan.*/
    private void initAutomaton() {
        end = 0;
        start = Integer.MAX_VALUE;
        
        listGenerationNumber = 0;
        
        endFound = false;
        starting.setLastList(-1);
        terminal.setLastList(-1);
    
        resetQueues();
        resetStates();
        addState(starting, new StateContainer(null, 0, maxCounters), null);
        swapQueues();
    }
    
    /**Asettaa käsiteltävien ja seuraavaksi käsiteltävien tilojen jonot tyhjiksi.*/
    private void resetQueues() {
        current = new Queue<StateContainer>();
        next = new Queue<StateContainer>(); 
    }
    
    /**Nollaa kaikkien tila-automaatin sisältämien tilojen lastList-muuttujan*/
    private void resetStates() {
        for (int i = 0; i < allStates.size(); i++) {
            State state = allStates.get(i);
            state.setLastList(-1);
        }
    }
    
    
    /**
     * Tutkii toteuttaako mikään kohta merkkijonosta säännöllistä lauseketta.
     * @param s tutkittava merkkijono
     * @return true, jos merkkijono toteuttaa säännöllisen lausekkeen, muulloin false
     */
    public boolean match(String s) {
        return find(0, s);
    }
    
    /**
     * Tutkii toteuttaako merkkijono alkaen kohdasta k säännöllistä lauseketta.
     * @param str tutkittava merkkijono
     * @param k alkukohta
     * @return true, jos merkkijono toteuttaa säännöllisen lausekkeen, muulloin false
     * @throws IllegalStateException jos alkutilaa ei ole
     */
    public boolean find(int k, String str) {
        
        if (starting == null) {
            throw new IllegalStateException();
        }
        
        initAutomaton();
        
        int index = k;

        for (index = k; index < str.length(); index++) {
            char c = str.charAt(index);
            step(c, index);
            
            if (deadEnd && endFound) {
                return true;
            }
             
            starting.setLastList(-1);
            addState(starting, new StateContainer(null, 0, maxCounters), null);       
            swapQueues();
        }
        
        if (isMatch(index)) {
            endFound = true;
        }
        
        return endFound;
    }
    
     
    /**
     * Asettaa tila-automaatin vuorossa olevaan tilaan ja tarkistaa päästäänkö tilasta
     * etenemään seuraaviin tiloihin käsiteltävällä merkillä c.
     * @param c käsiteltävä merkki
     * @param index käsiteltävän merkin indeksi
     */
    private void step(char c, int index) {
        
        deadEnd = true;
        listGenerationNumber++;
        
        while(!current.isEmpty()) {

            StateContainer container =  current.dequeue();
            State state = container.getState();
            
            int numEdges = state.getEdges().size();
            
            if (state.getType() == 0) {
                savePoint(index, container.getPathLength());
                endFound = true;
                terminal.setLastList(-1);
            }
                                  
            for (int j = 0; j < numEdges; j++) {
                Edge edge = state.getEdges().get(j);
                
                if (edge.matches(c)) {
                    
                    State following = edge.getTarget();
                    StateContainer newContainer = new StateContainer(following, container);
                    newContainer.incPathLength();
                    addState(following, newContainer, edge);
                    deadEnd = false;
                }
            }
            
        }
    }
    
    /**
     * Lisää tilan seuraavaksi käsiteltävien tilojen joukkoon. Jos tila on haarautuva tila (2),
     * hakee tyhjien kaarien päässä sijaitsevat. Jos tila on laskuritila (3), kutsutaan addCountingState()
     * -metodia. Jos tila on on jo lisätty käsiteltäviin tiloihin ja se ei sisälly laskuritilaan liittyvään
     * silmukkaan, tilaa ei lisätä käsiteltäviin tiloihin toiseen kertaan.
     * @param state lisättävä tila
     * @param prevContainer
     * @param incoming
     */
    private void addState(State state, StateContainer prevContainer, Edge incoming) {

        if (state == null || (state.getType() != 3 && !prevContainer.hasCounters() && state.getLastList() == listGenerationNumber)) {
            return;
        }

        state.setLastList(listGenerationNumber);

        if (state.getType() == 2) {
            List<Edge> list = state.getEdges();

            for (int i = 0; i < list.size(); i++) {
                Edge edge = list.get(i);
                addState(edge.getTarget(), prevContainer, edge);
            }
            return;

        } else if (state.getType() == 3) {

            addCountingState(state, prevContainer, incoming);
            return;

        }
        StateContainer cont = new StateContainer(state, prevContainer);
        next.enqueue(cont);

    }
    
    /**Kasvattaa laskuritilan säiliön laskuria, jos tilaan saavutaan laskettavasta tilasta, tai lisää säiliöön
     * uuden laskurin, jos tilaan saavutaan muuta reittiä. Lisää seuraavaksi käsiteltäviin tiloihin laskettavan tilan
     * sekä rajoitetun seuraavan tilan, jos laskuri on riittävän suuri.
     * @param state käsiteltävä laskuritila
     * @param prevContainer laskuritilan edellinen säiliö, joka sisältää laskurin
     * @param incoming kaari, jota pitkin tilaan saavutaan
     */
    private void addCountingState(State state, StateContainer prevContainer, Edge incoming) {
        StateContainer tempContainer = new StateContainer(null, prevContainer);

        if (incoming.isCounting()) {
            tempContainer.increaseCounter();
        } else {
            tempContainer.addCounter();
        }

        List<Edge> list = state.getEdges(tempContainer.getCounter());

        if (list.size() > 0) {
            Edge free = list.get(0);
            addState(free.getTarget(), tempContainer, free);
        }
        if (list.size() > 1) {
            Edge restricted = list.get(1);
            tempContainer.removeCounter();
            addState(restricted.getTarget(), tempContainer, restricted);
        }
    }
       
    /**
     * Tallentaa säännöllisen lausekkeen toteuttavan merkkijonon sijainnin luettavan
     * merkin sijainnin perusteella.
     * @param index luettavan merkin indeksi
     * @param count kuljetus polun pituus
     */
    private void savePoint(int index, int count) {
        int length = count;
        if ((length > 0) && (index - length <= start) && (index - length >= 0)) {
            end = index;
            start = end - count;
        }
    }
    
    /**
     * Vaihtaa tilalistojen viittaukset.
     */
    private void swapQueues() {
        Queue<StateContainer> d = next;
        next = current;
        current = d;       
    }
    /**
     * Kertoo onko käsiteltävien tilojen joukossa päätöstila. kutsuu savePoint()-
     * metodia, jos päätöstila on tilojen joukossa.
     * @param index viimeisen käsitellyn merkin indeksi
     * @return jos päätöstila on tilojen joukossa
     */
    private boolean isMatch(int index) {
        while (!current.isEmpty()) {
            StateContainer container = current.dequeue();
            State s = container.getState();
            if (s.getType() == 0) {
                savePoint(index, container.getPathLength());
                return true;
            }
        }
        return false;
    }
}
