/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package regex.matcher;

import regex.io.StringReader;
import java.util.Arrays;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author heidi
 */
public class StringReaderTest {
    
    StringReader reader;
    
    
    @Before
    public void setUp() {
        reader = new StringReader("abcde");
    }
    
    @Test
    public void readerHasCharInTheBeginning() {
       assertTrue(reader.hasChar()); 
    }
    
    @Test
    public void readerHasNotCharInTheEnd() {
        for (int i = 0; i < 5; i++) {
            reader.getChar();
        }
       assertFalse(reader.hasChar()); 
    }
    
    @Test
    public void getCharReturnsCharactersOneByOne() {
        char[] list = new char[5];
        for (int i = 0; i < 5; i++) {
            char c = reader.getChar();
            list[i] = c;
        }
        assertEquals(Arrays.toString(list), "[a, b, c, d, e]");
    }
}
