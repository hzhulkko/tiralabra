
package regex.domain;

import regex.domain.Edge;
import regex.utils.List;

/**
 *Luokka kuvaa tila-automaatin tilaa. Tiloja on kolmea tyyppiä lähtevien kaarien
 * lukumäärän mukaan:
 * 0 - päätöstila
 * 1 - perustila
 * 2 - haarautuva tila
 * 3 - laskuritila (lähteviä kaari todellisuudessa 0-2)
 */
public class State {
    /**Lista lähtevistä kaarista*/
    private List<Edge> edges;
    /**Tilan tyyppi*/
    private int type;
    /**Tallentaa sen jonotunnuksen, kun tila lisätään jonoon*/
    private int lastList;
    
    /**
     * Konstruktori saa parametrina tilan tyypin.
     * @param type tilan tyyppi
     */
    public State(int type) {
        this.edges = new List<Edge>(2);
        this.type = type;
        this.lastList = -1;

    }
      
    /**
     * Lisää tilaan lähtevän kaaren.
     * @param edge lähtevä kaari
     * @see Edge
     */
    public void add(Edge edge) {
        edges.add(edge);
    }
    
    /**
     * Palauttaa listan lähtevistä kaarista.
     * @return lista kaarista
     */
    public List<Edge> getEdges(int counter) {
        return edges;
    }
        /**
     * Palauttaa listan lähtevistä kaarista.
     * @return lista kaarista
     */
    public List<Edge> getEdges() {
        return edges;
    }
    /**
     * Palauttaa tilan tyypin.
     * @return tilan tyyppi.
     */
    public int getType() {
        return type;
    }
      
    public State copy() {
        State s = new State(this.type);
        for (int i = 0; i < edges.size(); i++) {
            s.add(edges.get(i));
        }
        return s;
        
    }
    
    /**
     * Palauttaa sen jono tunnuksen, jolla tila oli viimeksi.
     * @return viimeisimmän jonon tunnuksen tai -1, jos tilaa ei ole lisätty jonoon.
     */
    public int getLastList() {
        return lastList;
    }
    /**
     * Tallentaa tilaan jonotunnuksen
     * @param lastList jonotunnus, 0 tai suurempi kun tila on lisätty jonoon, -1 kun
     * jonotunnus nollataan.
     */
    public void setLastList(int lastList) {
        this.lastList = lastList;
    }
    
    
    
}
