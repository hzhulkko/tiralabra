
package regex.matcher;

/** Rajapinta kuvaa säännöllisten lausekkeen tulkin toiminnan*/

public interface RegexMatcher {
    /** Palauttaa säännöllisen lausekkeen toteuttavan merkkijonon viimeistä merkkiä
     * seuraavan merkin indeksin
     * @return  merkkijonon viimeinen indeksi + 1
     */
    int end();   
    /** 
     * Kertoo onko rivillä indeksistä i lähtien säännöllisen lausekkeen toteuttava merkkijono
     * @return true, jos rivillä on toteuttava merkkijono, muulloin false
     */ 
    boolean find(int i, String line);
    /**
     * Palauttaa säännöllisen lausekkeen toteuttavan merkkijonon ensimmäisen merkin indeksin
     * @return merkkijonon ensimmäinen indeksi
     */
    int start();
    /**
     * Asettaa tulkille uuden säännöllisen lausekkeen.
     * @param regex säännöllinen lauseke
     */
    void setRegex(String regex);
    
}
