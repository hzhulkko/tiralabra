/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package regex.matcher;

import regex.domain.Edge;
import regex.io.StringReader;
import regex.domain.NullEdge;
import regex.domain.State;
import regex.utils.List;
import regex.utils.Stack;

/**
 * Luokka toteuttaa tila-automaatin pääjäsentelijän, joka luo tila-automaatin 
 * säännöllisen lausekkeen perusteella. MainParser järjestelee tila-automaatin 
 * osia ja kutsuu tarvittaessa lisäosia Parser-rajapinnan toteuttavilta apujäsentelijöitä.
 * MainParser vastaa sulkulasekeiden () ja perusoperaattoreiden ?, *, + ja | tulkinnasta.
 * 
 * [ -merkin kohdalla jäsentelijä luovuttaa säännöllisen lausekkeen tulkinnan ja luennan
 * GroupParser-apujäsentelijälle.
 * 
 * { -merkin kohdalla jäsentelijä luovuttaa säännöllisen lausekkeen tulkinnan ja luennan
 * CountParser-apujäsentelijälle.
 * 
 * Muiden merkkien kohdalla MainParser kutsuu LiteralParser-jäsentelijää.
 * 
 * MainParser kerää myös kaikki automaatin tilat listaan ja pitää lukua laskuritilojen määrästä.
 * 
 * @see Parser
 */


public class MainParser {
    
    /**Sulkulausekkeen tavoin käsiteltäviuen lausekkeiden lukumäärä*/
    private int virtualParens;
    /**Käsiteltävien osien lukumäärä pinossa*/
    private int stackLevel;
    /**Pino tila-automaatin osille*/
    private Stack<Fragment> stack;
    /**Pino aikaisemmille käsiteltävien osien lukumäärille*/
    private Stack<Integer> prevLevels;
    /**Käytettävä apujäsentelijä*/
    private Parser parser;
    /**Säännöllisen lausekkeen lukija*/
    private StringReader reader;
    /**Tila-automaatin päätöstila*/
    private State terminal;
    /**Laskuritilojen lukumäärä*/
    private int counting;
    /**Lista kaikista tiloista*/
    private List<State> allStates;
    
    public MainParser(StringReader reader) {
        this.stack = new Stack<Fragment>();
        this.prevLevels = new Stack<Integer>();
        this.counting = 0;
        this.reader = reader;
        this.virtualParens = 0;
        this.stackLevel = 0;
        this.parser = new LiteralParser(this.reader);
        this.terminal = null;
        this.allStates = new List<State>(50);
    }
    
    /**Lukee säännöllistä lauseketta merkki kerrallaan*/
    public void read() {
        while (reader.hasChar()) {
            parse(reader.getChar());
        }
        
    }
    /**
     * Järjestelee osia käsiteltävän merkin mukaan. Antaa säännöllisen lausekkeen
     * lukemisen tarvittaessa apujäsentelijöiden tehtäväksi.
     * @param c käsiteltävä merkki
     */
    public void parse(char c) {
        State state;
        Fragment frag1, frag2;
            switch (c) {
                case '{':
                    parser = new CountParser(reader);
                    parser.read();
                    
                    frag1 = parser.getFragment();
                    frag2 = stack.pop();
                    
                    List<Edge> incoming = frag2.getOut();
                    for (int i = 0; i < incoming.size(); i++) {
                        Edge e = incoming.get(i);
                        e.makeCounting();
                    }
                    
                    state = frag1.getStart();
                    state.add(new NullEdge(frag2.getStart()));
                    frag2.patch(state);
                    
                    NullEdge restricted = new NullEdge();
                    state.add(restricted);

                    counting++;
                    
                    allStates.add(state);
                    stack.push(new Fragment(state, restricted));
                    break;
                case '[':
                    if (stackLevel > 1) {
                        patch();
                    }
                    parser = new GroupParser(reader);
                    parser.read();
                    frag1 = parser.getFragment();
                    
                    allStates.add(frag1.getStart());
                    stack.push(frag1);
                    stackLevel++;
                    break;
                case '(':
                    prevLevels.push(stackLevel);
                    stackLevel = 0;
                    break;
                case ')':
                    while (stackLevel > 1) {
                        patch();
                    }
                    while (virtualParens > 0) {
                        patch();
                        virtualParens--;
                    }
                    stackLevel = prevLevels.pop() + 1;
                    break;
                case '|':
                    if (stackLevel > 1) {
                        patch();
                    }
                    frag1 = stack.pop();
                    frag1.setCatenative(false);
                    stack.push(frag1);
                    stackLevel = 0;
                    virtualParens++;
                    break;
                case '?':
                    frag1 = stack.pop();
                    state = new State(2);
                    state.add(new NullEdge(frag1.getStart()));
                    state.add(new NullEdge());

                    frag2 = new Fragment(state);
                    frag2.append(frag1.getOut());
                    
                    allStates.add(state);
                    stack.push(frag2);
                    break;
                case '+':
                    frag1 = stack.pop();
                    state = new State(2);
                    state.add(new NullEdge(frag1.getStart()));
                    state.add(new NullEdge());
                    frag1.patch(state);
                    
                    allStates.add(state);
                    stack.push(new Fragment(frag1.getStart(), state.getEdges()));
                    break;
                case '*':
                    frag1 = stack.pop();
                    state = new State(2);
                    state.add(new NullEdge(frag1.getStart()));
                    state.add(new NullEdge());
                    frag1.patch(state);
                    
                    allStates.add(state);
                    stack.push(new Fragment(state));
                    break;

                default:
                    if (stackLevel > 1) {
                        patch();
                    }
                    parser = new LiteralParser(reader);
                    parser.parse(c);
                    frag1 = parser.getFragment();
                    
                    stack.push(frag1);
                    allStates.add(frag1.getStart());
                    
                    stackLevel++;
                    break;
            }
    }
    
    /**
     * Metodi liittää osia toisiinsa muodostaen suurempia osakokonaisuuksia. 
     * Katenoituvat osat liitetään peräkkäin. Jos katenoituvaan osan perään liitetään
     * ei katenoituva osa, syntyvä kokonaisuus merkitään katenoitumattomaksi. 
     * Katenoitumattoman osaan muut osat liitetään rinnakkain muodostaen haaran.
     * Jos myös linnalle liitettävä osa ei ole katenoituva, kokonaisuus merkitään 
     * katenoitumattomaksi.
     */

    public void patch() {
        Fragment frag1 = stack.pop();
        Fragment frag2 = stack.pop();
        Fragment frag3;
        if (!frag2.isCatenative()) {
            frag3 = createSplit(frag1, frag2);
        } else {
            frag2.patch(frag1.getStart());
            frag3 = new Fragment(frag2.getStart(), frag1.getOut());
        }
        if (!frag1.isCatenative()) {
            frag3.setCatenative(false);
        }
        stack.push(frag3);
        stackLevel--;
    }
    
    /**Palauttaa valmiin tila-automaatin*/
    public Fragment getFragment() {
        while (stack.size() > 1) {
            patch();
        }
        terminal = new State(0);
        allStates.add(terminal);
        
        Fragment fragment = stack.pop();
        fragment.patch(terminal);
        Fragment start = createStartFragment();
        start.patch(fragment.getStart());
        return new Fragment(start.getStart());
    }
    
    /**
     * Palauttaa laskuritilojen lukumäärän.
     * @return laskuritilojen lukumäärä, 0 jos laskuritiloja ei ole.
     */
    public int getCountingStates() {
        return counting;
    }
    
    /**
     * Palauttaa viitteen lopputilaan.
     * @return lopputila
     */
    public State getTerminal() {
        return terminal;
    }
    
    /**
     * Palauttaa listan kaikista tiloista.
     * @return lista kaikista tiloista
     */
    public List<State> getAllStates() {
        return allStates;
    }
    
    /**
     * Luo tyypin 2 alkutilan, josta lähtee kuitenkin vain yksi tyhjä kaari.
     * @return alkutilan sisältävä fragmentti
     */
    private Fragment createStartFragment() {
        State state = new State(2);
        state.add(new NullEdge());
        allStates.add(state);
        return new Fragment(state);
    }
       
    /**
     * Luo haarautuvan tilan, josta lähtee kaksi tyhjää kaarta vaihtoehtoisiin
     * osiin. Sitoo osat haaratilaan ja palauttaa yhtenäisen haarautuvan osan.
     * @param frag1 ensimmäinen vaihtoehtoinen osa
     * @param frag2 toinen vaihtoehtoinen osa
     * @return haarautuva osa
     */
    private Fragment createSplit(Fragment frag1, Fragment frag2) {
        State state = new State(2);
        state.add(new NullEdge(frag1.getStart()));
        state.add(new NullEdge(frag2.getStart()));
        Fragment f3 = new Fragment(state);
        f3.append(frag1.getOut());
        f3.append(frag2.getOut());
        return f3;
    }
    
    
}
