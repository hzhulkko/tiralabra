
package regex.matcher;

import regex.io.StringReader;
import regex.domain.State;

public class Nfa {

    /**Tila-automaatin lukija*/
    private NfaMatcher matcher;
    /**Tila-automaatin rakentaja*/
    private MainParser parser;
    
    
    public Nfa() {
        this.matcher = null;
        this.parser = null;
    }
    /**
     * Muodostaa tila-automaatin säännöllisen lausekkeen perusteella ja luo tila-
     * automaatin lukijan. Luovuttaa tila-automaatin lukijalle automaatin alku- ja lopputilan sekä
     * listan kaikista tiloista.
     * @param regex säännöllinen lauseke
     */
    public void compile(String regex) {
        StringReader reader = new StringReader(regex);
        parser = new MainParser(reader);
        parser.read();
        Fragment fragment = parser.getFragment();
        
        matcher = new NfaMatcher(fragment.getStart(), parser.getTerminal(), parser.getAllStates(), parser.getCountingStates());
    }
    /**
     * Kutsuu tila-automaatin lukijan match-metodia. Metodi testausta varten.
     * @param s luettava merkkijono
     * @return true, jos merkkijono toteuttaa 
     */
    public boolean match(String s) {
       if (matcher == null) {
           throw new IllegalStateException();
       }
       return matcher.match(s);

    }

    public NfaMatcher matcher() {
        return matcher;
    }
    
    

    

    
    
    
    
}
