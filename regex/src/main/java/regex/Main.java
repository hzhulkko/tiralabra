package regex;

import java.io.File;
import java.util.Scanner;
import regex.io.LineReader;
import regex.matcher.CustomMatcher;
import regex.matcher.StandardMatcher;

public class Main {
    
    public static void read(MatchFinder finder, String regex) {
            finder.setRegex(regex);
            long start = System.currentTimeMillis();
            finder.read();
            long stop = System.currentTimeMillis();
            System.out.println("Search time: " + (stop - start) + " ms");
            System.out.println("Regular expression: " + regex);
    }

    public static void main(String[] args) {

        String path = "kalevala-test.txt";
        String regex;
        boolean testRun = false;
        
        if (args.length == 0) {
            ATesting.test(1,5,10,15,20,25);
            testRun = true;
            return;
        } else {
            path = args[0];
            regex = ":q";
            
        }
        
        File file = new File(path);
        LineReader reader = new LineReader(file);
        MatchFinder finder = new MatchFinder(new CustomMatcher(), reader);
        Scanner scanner = new Scanner(System.in);


        while (true) {
            if (!testRun) {
                System.out.println("Regular expression (type ':q' to quit) > ");
                regex = scanner.nextLine();
            }

            if (regex.equals(":q")) {
                break;
            }
            read(finder, regex);
            finder.print();
            
            if (testRun) {
                break;
            }

        }


    }
}
    
    
