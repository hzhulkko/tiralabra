/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package regex.matcher;

import regex.domain.NullEdge;
import regex.domain.State;
import regex.utils.List;
import regex.utils.Stack;


public interface Parser {
    
    void read();
        
    void parse(char c);
    
    Fragment getFragment();      
}
