/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package regex.utils;

import java.util.Arrays;
import static org.junit.Assert.*;
import org.junit.*;

public class QueueTest {
    
    private Queue<Integer> q;

    @Before
    public void setUp() {
        q = new Queue<Integer>();     
    }

    @Test
    public void constructorCreatesEmptyQueue() {
        assertTrue(q.isEmpty());
    }
    
    @Test
    public void queueIsNotEmptyAfterEnqueue() {
        q.enqueue(1);
        assertFalse(q.isEmpty());
    }
    
    @Test
    public void enqueueIncreasesQueueSize() {
        q.enqueue(1);
        assertEquals(1, q.size());
    }
    
    @Test
    public void enqueueIncreasesQueueSize2() {
        q.enqueue(1);
        q.enqueue(2);
        assertEquals(2, q.size());
    }
    
    @Test(expected = NullPointerException.class)
    public void dequeueThrowsException() {
        q.dequeue();
    }
    
    @Test
    public void dequeueDecreasesQueueSize() {
        q.enqueue(1);
        q.enqueue(2);
        q.dequeue();
        assertEquals(1, q.size());
    }
    
    @Test
    public void dequeueReturnsFirstInQueue() {
        q.enqueue(1);
        q.enqueue(2);
        int first = q.dequeue();
        assertEquals(1, first);
    }
    
    @Test
    public void dequeueReturnsItemsInRightOrder() {
        for (int i = 1; i <= 10; i++) {
            q.enqueue(i);
        }
        int[] list = new int[10];
        for (int i = 0; i < 10; i++) {
            int k = q.dequeue();
            list[i] = k;
        }
        assertEquals(Arrays.toString(list), "[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]");
    }
    
    
    // AIKAVAATIVUUSTESTIT
    
    @Test
    public void enqueueTimeTest() {
        double average = calcAverageEnqueueTime(100);
        double delta = average;
        
        double test = calcAverageEnqueueTime(100);
        
        assertEquals(average, test, delta);
        
        test = calcAverageEnqueueTime(1000);
        
        assertEquals(average, test, delta);
        
        test = calcAverageEnqueueTime(10000);
        
        assertEquals(average, test, delta);
        
        test = calcAverageEnqueueTime(100000);
        
        assertEquals(average, test, delta);
        
        test = calcAverageEnqueueTime(100000);
        
        assertEquals(average, test, delta);

    }
    
    @Test
    public void dequeueTimeTest() {
        double average = calcAverageDequeueTime(100);
        double delta = average;
        
        double test = calcAverageDequeueTime(100);
        
        assertEquals(average, test, delta);
        
        test = calcAverageDequeueTime(1000);
        
        assertEquals(average, test, delta);
        
        test = calcAverageDequeueTime(10000);
        
        assertEquals(average, test, delta);
        
        test = calcAverageDequeueTime(100000);
        
        assertEquals(average, test, delta);
        
        test = calcAverageDequeueTime(1000000);
        
        assertEquals(average, test, delta);
        
    }
    
    
    
    private double calcAverageEnqueueTime(int max) {
        Queue<Integer> queue = new Queue<Integer>();
        
        double sum = 0.0;
        
        for (int n = 0; n < max; n++) {
            double start = getCurrentTime();
            queue.enqueue(1);
            double stop = getCurrentTime();
            sum += stop - start;      
        }
        return sum / max;
    }
    
    private double calcAverageDequeueTime(int max) {
        Queue<Integer> queue = new Queue<Integer>();
        
        for (int n = 0; n < max; n++) {
            queue.enqueue(1);
        }
        
        double sum = 0.0;
        
        for (int n = 0; n < max; n++) {
            double start = getCurrentTime();
            queue.dequeue();
            double stop = getCurrentTime();
            sum += stop - start;      
        }
        return sum / max;
    }
    
    private double getCurrentTime() {
        return System.nanoTime();
    }

}
