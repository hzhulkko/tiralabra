
package regex.utils;

/**Luokka toteuttaa geneerisen solmun, jonka avulla voi muodostaa linkitettyjä
 * listoja T tyyppisistä alkioista. */

public class Node<T> {
    
    private T object;
    private Node<T> prev;
    private Node<T> next;
    
    /**Konstruktori saa parametrina viitteeen linkitettävään objektiin.*/
    public Node (T t) {
        this.object = t;
        this.prev = null;
        this.next = null;
    }
    /**Palauttaa viitteen solmun säilömään alkioon*/
    public T get() {
        return object;
    }
    /**Palauttaa viitteen edelliseen solmuun*/
    public Node<T> getPrev() {
        return prev;
    }
    /**Asettaa solmulle edeltäjän*/
    public void setPrev(Node<T> prev) {
        this.prev = prev;
    }
    /**Palauttaa viitteen seuraavaan solmuun*/
    public Node<T> getNext() {
        return next;
    }
    /**Asettaa solmulle seuraajan*/
    public void setNext(Node<T> next) {
        this.next = next;
    }
    
    
}
