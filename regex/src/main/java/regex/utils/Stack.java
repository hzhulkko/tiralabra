/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package regex.utils;

/**
 * Luokka toteuttaa geneerisen pinon linkitetyn listan avulla. Linkitetty lista 
 * muodostuu toisiinsa liittyneistä Node-luokan solmuista, jotka sisältävät
 * viittet pinottavaan T-tyypin olioon.
 *  
 * @param <T> 
 */

public class Stack<T> {
    /**Lisättyjen alkioiden lukumäärä*/
    private int size;
    /**Pinon alin (ensimmäiseksi lisätty) alkio*/
    private Node<T> bottom;
    /**Pinon ylin (viimeiseksi lisätty) alkio*/
    private Node<T> top;

    public Stack() {
        this.size = 0;
        this.bottom = new Node<T>(null);
        this.top = bottom;
    }
    /**
     * Liittää tyypin T alkion pinon päällimäiseksi.
     */
    public void push(T t) {
        Node<T> item = new Node<T>(t);
        top.setNext(item);
        item.setPrev(top);
        size++;
        top = item;
    }
    /**
     * Poistaa ja palauttaa pinon päällimmäisen alkion.
     */
    public T pop() {
        if (isEmpty()) {
            throw new NullPointerException();
        }
        Node<T> removed = top;
        Node<T> prev = removed.getPrev();
        top = prev;
        size--;
        return removed.get();
    }
   /**
    * Kertoo onko pino tyhjä.
    * @return true jos pinossa ei ole alkiota, muulloin false */
   public boolean isEmpty() {
       return bottom == top;
   }
   /** 
    * Palauttaa pinossa olevien alkioiden lukumäärän
    * @return alkioiden lukumäärä, 0 kun pino on tyhjä */
   public int size() {
       return size;
   }

    
}
