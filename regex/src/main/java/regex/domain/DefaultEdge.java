
package regex.domain;

/**Luokka kuvaa tilojen välistä kaarta, joka vastaa yhtä merkkiä*/

public class DefaultEdge implements Edge {
    /**Kaaren merkki*/
    private char c;
    /**Kaaren kohdetila*/
    private State target;
    /**Ilmaisee, onko kaari osa laskuritilaan liittyvää silmukkaa*/
    private boolean counting;
    
    /**
     * Konstruktorissa kaarelle asetetaan merkki ja kohdetila.
     * @param c kaaren merkki
     * @param s kohdetila
     */
    public DefaultEdge(char c, State s) {
        this.c = c;
        this.target = s;
        this.counting = false;
    }
    
    /**
     * Konstruktori luo kaaren, jolla on merkki mutta ei ole kohdetilaa.
     * @param c kaaren merkki.
     */
    public DefaultEdge(char c) {
        this(c, null);
    }
    
    /**
     * Palauttaa kaaren merkin.
     * @return kaaren merkki
     */
    @Override
    public char getC() {
        return c;

    }
    /**
     * Palauttaa kaaren kohdetilan.
     * @return kohdetila
     * @see State
     */
    @Override
    public State getTarget() {
        return target;
    }
    /**
     * Asettaa kaarelle kohdetilan.
     * @param target kohdetila
     * @see State
     */
    @Override
    public void setTarget(State target) {
        this.target = target;
    }

    @Override
    public boolean matches(char c) {
        return this.c == c;
    }

    @Override
    public void makeCounting() {
        this.counting = true;
    }

    @Override
    public boolean isCounting() {
        return this.counting;
    }

}
