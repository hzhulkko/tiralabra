
package regex.domain;

/**Luokka sisältää säännöllisen lausekkeen toteuttavan merkkijonon ja 
 * sen sijainnin tiedostossa.
 */

public class Match {
    
    /** Rivin numero*/
    private int line;
    /** Indeksi, josta merkkijono alkaa */
    private int start;
    /** Indeksi, johon merkkijono loppuu */
    private int end;
    /** Säännöllisen lausekkeen toteuttva merkkijono */ 
    private String match;
    /**
     * Luo merkkijonon ja merkkijonon sijainnin sisältävän Match-olion.
     * @param line rivi, jolla merkkijono sijaitsee
     * @param start merkkijonon alkuindeksi
     * @param end merkkijonon loppuindeksi
     * @param match merkkijono
     */
    public Match(int line, int start, int end, String match) {
        this.line = line;
        this.start = start;
        this.end = end;
        this.match = match;
    }
    /**Palauttaa indeksin, josta merkkijono alkaa
     *@return indeksi, josta merkkijono alkaa
     */
    public int getStart() {
        return start;
    }
    /**Palauttaa indeksin, johon merkkijono loppuu
     * @return indeksi, johon merkkijono päättyy
     */
    public int getEnd() {
        return end;
    }
    /**Palauttaa merkkijonon rivin tiedostossa
     *@return rivi. jolla merkkijono sijaitsee
     */
    public int getLine() {
        return line;
    }
    /**Palauttaa säännöllisen lausekkeen toteuttavan merkkijonon
     * @return merkkijono
     */
    public String getMatch() {
        return match;
    }
    
    @Override
    public String toString() {
        return "Line " + line + " indeces " + start + "..." + end + ": " + match;
    }
    
    
}
