
package regex.domain;

import regex.domain.State;
import regex.domain.Edge;

/**
 * Luokka kuvaa tyhjän kaaren.
 */

public class NullEdge implements Edge {
    /**Kaaren kohdetila*/
    private State target;
    /**Ilmaisee, onko kaari osa laskuritilaan liittyvää silmukkaa*/
    private boolean counting;
    
    /**
     * Konstruktori sa parametrina kaaren kohdetilan.
     * @param target kohdetila
     */
    public NullEdge(State target) {
        this.target = target;
        this.counting = false;
    }
    
    public NullEdge() {
        this(null);
    }
    /**
     * Palauttaa tyhjän merkin.
     * @return ' '
     */
    @Override
    public char getC() {
        return ' ';
    }

    @Override
    public State getTarget() {
        return target;
    }

    @Override
    public void setTarget(State target) {
        this.target = target;
    }
    /**
     * Palauttaa aina true.
     * @param c 
     * @return true
     */
    @Override
    public boolean matches(char c) {
        return true;
    }

    @Override
    public void makeCounting() {
        this.counting = true;
    }

    @Override
    public boolean isCounting() {
        return this.counting;
    }

}
