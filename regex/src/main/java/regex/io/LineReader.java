
package regex.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/** LineReader lukee tiedostoja rivi kerrallaan*/
public class LineReader {
    /**Tiedoston lukija*/
    private BufferedReader reader;
    /**Luettava tiedosto*/
    private File file;
    /**Luettujen rivien lukumäärä*/
    private int lines;
    
    /**
     * Luo lukijan paprametrina annetulle tiedostolle.
     * @param file luettava tiedosto
     */
    public LineReader(File file) {
        this.file = file;
        this.lines = 0;
        try {
            this.reader = new BufferedReader(new FileReader(file));
        } catch (FileNotFoundException ex) {
            System.out.println("File not found");;
        }
    }
    
    /**
     * Lukee rivin tiedostosta ja palauttaa sen String-muodossa.
     * @return luettu rivi
     * @throws IOException jos tiedoston lukeminen ei onnistu
     */
    public String readLine() {
        if (file.length() > 0) {
            try {
                String line = reader.readLine();
                if (line != null) lines++;
                return line;
            } catch (IOException ex) {
                return null;
            }
        } else {
            return null;
        }
    }
    /**
     * Palauttaa luettujen rivien lukumäärän.
     * @return luetut rivit
     */
    public int getLineNumber() {
        return lines;
    }
    
    /**
     * Palauttaa tiedoston nimen.
     * @return tiedoston nimi
     */
    public String getFile() {
        return this.file.getPath();
    }
    /**
     * Palauttaa lukijan tiedoston alkuun ja nollaa luettujen rivien lukumäärän.
     */
    public void reset() {
        try {
            reader = new BufferedReader(new FileReader(file));
            lines = 0;
        } catch (FileNotFoundException ex) {
            System.out.println("File not found");
        }
    }
    
}
